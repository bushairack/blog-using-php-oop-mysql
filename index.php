<?php
#***************************************************************************************#


				/**
				*
				*	@file 				Start Page with Login credentials
				*	@author 				Bushaira Chakkikalathil <abcd@ab.de>
				*	@copyright			Project-PHP - mySQL - Datenbankanbindung
				*	@lastModifyDate	2022-05-11
				*	
				*/


#***************************************************************************************#


				#***********************************#
				#********** CONFIGURATION **********#
				#***********************************#
				
				require_once('include/config.inc.php');
				require_once('include/db.inc.php');
				require_once('include/form.inc.php');
				include_once('include/dateTime.inc.php');
				include_once('include/authentification.inc.php');


#***************************************************************************************#

				#********** INCLUDE CLASSES **********#

				require_once('Class/User.class.php');
				require_once('Class/Category.class.php');
				require_once('Class/Blog.class.php');





#***************************************************************************************#

				#**************************************#
				#********** OUTOUT BUFFERING **********#
				#**************************************#
				
				// ob_start();
				if( ob_start() === false ) {
					// Fehlerfall
if(DEBUG)		echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER beim Starten des Output Bufferings! <i>(" . basename(__FILE__) . ")</i></p>\r\n";				
					
				} else {
					// Erfolgsfall
if(DEBUG)		echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Output Buffering erfolgreich gestartet. <i>(" . basename(__FILE__) . ")</i></p>\r\n";									
				}




#***************************************************************************************#


				#****************************************#
				#********** INITIALIZE SESSION **********#
				#****************************************#
				
				$login = checkLogin('blogProjectOOP');			
if(DEBUG_V)	echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$login: $login <i>(" . basename(__FILE__) . ")</i></p>\n";

				

#***************************************************************************************#

				#******************************************#
				#********** INITIALIZE VARIABLES **********#
				#******************************************#
				
				$loginError 			= NULL;
				$categoryFilterId		= NULL;



#***************************************************************************************#

				#*************************************#
				#********** TESTING CLASSES **********#
				#*************************************#

/*
				$UserObject = new User('1', 'ghhhj', 'hhjjj', 'hjk');
				$CategoryObject = new Category( '1', 'ghj' );
				$BlogObject = new Blog($UserObject, $CategoryObject, 'ghhhj', 'hhjjj', 'hjk');

*/		
				$BlogObject = new Blog(new User(), new Category() );
#***************************************************************************************#

				

				#***********************************************#
				#**************** PROCESS FORM  ****************#
				#***********************************************#
				
				#********** PREVIEW POST ARRAY **********#
/*
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($_POST);					
if(DEBUG_V)	echo "</pre>";
*/
				#****************************************#
				
				// Schritt 1 FORM: Prüfen, ob Formular abgeschickt wurde
				if( isset($_POST['formLogin']) ) {
if(DEBUG)		echo "<p class='debug'>🧻 <b>Line " . __LINE__ . "</b>: Formular 'Registration' wurde abgeschickt. <i>(" . basename(__FILE__) . ")</i></p>\n";										
					
					// Schritt 2 FORM: Werte auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte auslesen und entschärfen... <i>(" . basename(__FILE__) . ")</i></p>\n";
				
					#********** FETCH VALUES FOR OBJECT'S DATA **********#
					
					
																
																
					cleanString($BlogObject->getUser()->setUserEmail		(	$_POST['loginName']));
					cleanString($BlogObject->getUser()->setUserPassword	(	$_POST['loginPassword']));
					
					$loginName 		= cleanString($BlogObject->getUser()->getUserEmail()    );
					$loginPassword = cleanString($BlogObject->getUser()->getUserPassword() );
					
if(DEBUG_V)		echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)		print_r($BlogObject);					
if(DEBUG_V)		echo "</pre>";
					
					// Schritt 3 FORM: ggf. Werte validieren
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Feldwerte werden validiert... <i>(" . basename(__FILE__) . ")</i></p>\n";
					$errorLoginName 		= validateEmail		( $loginName );
					$errorLoginPassword 	= checkInputString	( $loginPassword );
					
					
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$errorLoginName: $errorLoginName <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$errorLoginPassword: $errorLoginPassword <i>(" . basename(__FILE__) . ")</i></p>\n";

					
					#********** FINAL FORM VALIDATION **********#					
					if( $errorLoginName OR $errorLoginPassword ) {
						// Fehlerfall
if(DEBUG)			echo "<p class='debug err'>Line <b>" . __LINE__ . "</b>: Formular enthält noch Fehler! <i>(" . basename(__FILE__) . ")</i></p>";						
						$loginError = 'Benutzername oder Passwort falsch!';
						
					} else {
						// Erfolgsfall
if(DEBUG)			echo "<p class='debug ok'>Line <b>" . __LINE__ . "</b>: Das Formular ist formal fehlerfrei. <i>(" . basename(__FILE__) . ")</i></p>";						
									
						// Schritt 4 FORM: Daten weiterverarbeiten
						
						#*********************************************************#
						#************ FETCH USER DETAILS FROM DATABASE ************#
						#*********************************************************#
						
if(DEBUG)			echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Fetching all user details from database... <i>(" . basename(__FILE__) . ")</i></p>\n";
						$PDO = DBConnect('bloog_oop');
						
						$userInfosArray = User::fetchFromDB($PDO, $loginName );
					
if(DEBUG_V)			echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)			print_r($userInfosArray);					
if(DEBUG_V)			echo "</pre>";
						
						#********** VERIFY LOGIN EMAIL **********#

						if( $userInfosArray['userEmail'] !== $loginName ){
							// Fehlerfall
if(DEBUG)				echo "<p class='debug err'>Line <b>" . __LINE__ . "</b>: FEHLER: Benutzername wurde nicht in DB gefunden! <i>(" . basename(__FILE__) . ")</i></p>";
							$loginError = 'Benutzername oder Passwort falsch!';	
						} else {
							// Erfolgsfall
if(DEBUG)				echo "<p class='debug ok'>Line <b>" . __LINE__ . "</b>: Benutzername wurde in DB gefunden. <i>(" . basename(__FILE__) . ")</i></p>";
							
							
							#********** VERIFY PASSWORD **********#							
							if( !password_verify( $loginPassword, $userInfosArray['userPassword']) ) {
								// Fehlerfall
if(DEBUG)					echo "<p class='debug err'>Line <b>" . __LINE__ . "</b>: FEHLER: Passwort stimmt nicht mit DB überein! <i>(" . basename(__FILE__) . ")</i></p>";
								$loginError = 'Benutzername oder Passwort falsch!';
							
							} else {
								// Erfolgsfall
if(DEBUG)					echo "<p class='debug ok'>Line <b>" . __LINE__ . "</b>: Passwort stimmt mit DB überein. LOGIN OK. <i>(" . basename(__FILE__) . ")</i></p>";
							
								#********** 4. PROCESS LOGIN **********#
if(DEBUG)					echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Login wird durchgeführt... <i>(" . basename(__FILE__) . ")</i></p>\n";
								
								#********** PREPARE SESSION **********#
								// Der Sessionname sollte den Domainnamen der Webseite (ohne www., .com, .de etc.) enthalten
								session_name('blogProjectOOP');
								
								#********** START SESSION **********#
								if( session_start() === false ) {
									// Fehlerfall
if(DEBUG)						echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER beim Starten der Session! <i>(" . basename(__FILE__) . ")</i></p>\n";				
										
								} else {
									// Erfolgsfall
if(DEBUG)						echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Session erfolgreich gestartet. <i>(" . basename(__FILE__) . ")</i></p>\n";				
										
										
										
										#********** SAVE USER DATA INTO SESSION **********#
										$_SESSION['IPAddress']			= $_SERVER['REMOTE_ADDR'];
										$_SESSION['ID'] 				= $userInfosArray['userID'];
										$_SESSION['userFirstName'] 	= $userInfosArray['userFirstName'];
										$_SESSION['userLastName'] 		= $userInfosArray['userLastName'];
										
									
										
										
if(DEBUG_V)							echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)							print_r($_SESSION);					
if(DEBUG_V)							echo "</pre>";

										#********** REDIRECT TO dashboard.php **********#
										header('LOCATION: dashboard.php');
										
										/*
											3. Fallback, falls Redirect nicht funktionieren sollte:
											exit beendet sofort die Ausführung des Skripts
										*/
										exit();	
								} // PREPARE SESSION END

							} // VERIFY PASSWORD END
							
						} //VERIFY LOGIN EMAIL END
		
					} // FINAL FORM VALIDATION END

				} // PROCESS FORM  END

#***************************************************************************************#

					#************************************************#
					#******** FETCH ALL BLOG ENTRIES FORM DB ********#
					#************************************************#
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Fetching all user details from database... <i>(" . basename(__FILE__) . ")</i></p>\n";
					$PDO = DBConnect('bloog_oop');
					$allCategoriesArray = Category::fetchAllFromDB( $PDO );
						
					// $allBlogArticlesArray = Blog::fetchAllFromDB( $PDO );
/*
if(DEBUG_V)		echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)		print_r($allBlogArticlesArray);					
if(DEBUG_V)		echo "</pre>";
*/
/*
foreach( $allCategoriesArray AS $categorySingleItemArray ){
	
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$categorySingleItemArray: '{$categorySingleItemArray->getCatLabel()}' <i>(" . basename(__FILE__) . ")</i></p>\n";

}
*/




#***************************************************************************************#

					

				#********************************************#
				#********** PROCESS URL PARAMETERS **********#
				#********************************************#
				
				// Schritt 1 URL: Prüfen, ob Parameter übergeben wurde
				if( isset($_GET['action']) ) {
if(DEBUG)		echo "<p class='debug'>🧻 <b>Line " . __LINE__ . "</b>: URL-Parameter 'action' wurde übergeben. <i>(" . basename(__FILE__) . ")</i></p>\n";										
			
					// Schritt 2 URL: Werte auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte werden ausgelesen und entschärft... <i>(" . basename(__FILE__) . ")</i></p>\n";
					$action = cleanString($_GET['action']);
if(DEBUG_V)		echo "<p class='debug value'>Line <b>" . __LINE__ . "</b>: \$action: $action <i>(" . basename(__FILE__) . ")</i></p>";
		
					// Schritt 3 URL: ggf. Verzweigung
							
							
					#********** LOGOUT **********#
					if( $_GET['action'] === 'logout' ) {
if(DEBUG)			echo "<p class='debug'>📑 Line <b>" . __LINE__ . "</b>: 'Logout' wird durchgeführt... <i>(" . basename(__FILE__) . ")</i></p>";	
						
						logout();
						
						
					#********** FILTER BY CATEGORY **********#					
					} elseif( $action === 'filterByCategory' ) {
if(DEBUG)			echo "<p class='debug'>📑 Line <b>" . __LINE__ . "</b>: Kategoriefilter aktiv... <i>(" . basename(__FILE__) . ")</i></p>";				
						

if(DEBUG_V)			echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$getAction: '{$_GET['catID']}' <i>(" . basename(__FILE__) . ")</i></p>\n";			

/*						
if(DEBUG_V)		echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)		print_r($allCategoriesArray);					
if(DEBUG_V)		echo "</pre>";						
*/						
						
						// 2. URL-Parameter auslesen
						
						cleanString($BlogObject->getCategory()->setCatID		(	$_GET['catID']));
						$categoryFilterId = $BlogObject->getCategory()->getCatID();

if(DEBUG_V)			echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$categoryFilterId: $categoryFilterId <i>(" . basename(__FILE__) . ")</i></p>\n";			
	

						if( !isset($categoryFilterId) )  {
							// Fehlerfall
if(DEBUG)				echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: Gewählte Kategorie-ID ist ungültig! <i>(" . basename(__FILE__) . ")</i></p>\n";
							$categoryFilterId = NULL;
						
						#********** VALID CATEGORY ID **********#
						} else {
							// Erfolgsfall
if(DEBUG)				echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Gewählte Kategorie-ID ist gültig. <i>(" . basename(__FILE__) . ")</i></p>\n";				
						}	
					} //FILTER BY CATEGORY END
			
					
				} //PROCESS URL PARAMETERS

#***************************************************************************************#

					
					
					#********** PREPARE SQL STATEMENT AND PLACEHOLDERS **********#
					
					// for both cases generate sql base statement
					$sql 		= 	'SELECT * FROM blog
									INNER JOIN user USING(userID)
									INNER JOIN category USING(catID)';
									
					$params 	= array();
						
						
						#********** A) FETCH ALL BLOG ENTRIES **********#
				
					if( !isset( $categoryFilterId ) ) {
if(DEBUG)			echo "<p class='debug'>📑 Line <b>" . __LINE__ . "</b>: Lade alle Blog-Einträge...$categoryFilterId<i>(" . basename(__FILE__) . ")</i></p>";

						#********** B) FILTER BLOG ENTRIES BY CATEGORY ID **********#				
					} else {
if(DEBUG)			echo "<p class='debug'>📑 Line <b>" . __LINE__ . "</b>: Filtere Blog-Einträge nach Kategorie-ID$categoryFilterId... <i>(" . basename(__FILE__) . ")</i></p>";					
					
						// add condition for category filter
						$sql		.=	' WHERE catID = ?';
						// assign placeholder
						$params 	 = array($BlogObject->getCategory()->getCatID());
					}
					
					// for both cases add 'order by' condition
					$sql		.= ' ORDER BY blogDate DESC';				
					#**************************************************************#
					
					// Schritt 1 DB: DB-Verbindung herstellen
					$PDO = DBConnect('bloog_oop');
					$allBlogArticlesArray = Blog::fetchAllFromDB( $PDO, $sql, $params );
/*					
if(DEBUG_V)		echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)		print_r($allBlogArticlesArray);					
if(DEBUG_V)		echo "</pre>";
*/

#***************************************************************************************#







?>

<!doctype html>

<html>

	<head>
		<meta charset="utf-8">
		<title>PHP-Projekt Blog</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/debug.css">
	</head>

	<body>
		
		<!-- ---------- PAGE HEADER START ---------- -->
		
		<header class="fright">
		
			<?php if( $login === false ): ?>
				<?php if($loginError): ?>
				<p class="error"><b><?= $loginError ?></b></p>
				<?php endif ?>

				<!-- -------- Login Form START -------- -->
				<form action="" method="POST">
					<input type="hidden" name="formLogin">
					<input type="text" name="loginName" placeholder="Email">
					<input type="password" name="loginPassword" placeholder="Password">
					<input type="submit" value="Login">
				</form>
				<!-- -------- Login Form END -------- -->
				
			<?php else: ?>
				<!-- -------- PAGE LINKS START -------- -->
				<a href="?action=logout">Logout</a><br>
				<a href='dashboard.php'>zum Dashboard >></a>
				<!-- -------- PAGE LINKS END -------- -->
			<?php endif ?>
			
		
		</header>
		
		<div class="clearer"></div>
				
		<br>
		<hr>
		<br>
		
		<!-- ---------- PAGE HEADER END ---------- -->
		
		
		
		<h1>PHP-Projekt Blog</h1>
		<p><a href='index.php'>:: Alle Einträge anzeigen ::</a></p>
		
		
		
		<!-- ---------- BLOG ENTRIES START ---------- -->
		
		<main class="blogs fleft" >
			
			<?php if( !$allBlogArticlesArray ): ?>
				<p class="info">Noch keine Blogeinträge vorhanden.</p>
			
			<?php else: ?>
		
				<?php foreach( $allBlogArticlesArray AS $singleBlogItemArray ): ?>
				
				
					<?php $dateTimeArray = isoToEuDateTime($singleBlogItemArray->getBlogDate()) ?>
					
					<article class='blogEntry'>
					
						<a name='entry<?= $singleBlogItemArray->getBlogID() ?>'></a>
						
						<p class='fright'><a href='?action=filterByCategory&catID=<?= $singleBlogItemArray->getCategory()->getCatID() ?>'>Kategorie: <?= $singleBlogItemArray->getCategory()->getCatLabel() ?></a></p>
	
						<h2 class='clearer'><?= $singleBlogItemArray->getBlogHeadline() ?></h2>
						<br>

						<p class='author'><?= $singleBlogItemArray->getUser()->getFullName() ?> (<?= $singleBlogItemArray->getUser()->getUserCity() ?>) schrieb am <?= $dateTimeArray['date'] ?> um <?= $dateTimeArray['time'] ?> Uhr:</p>
						
						<p  class='blogContent'>
						
							<?php if($singleBlogItemArray->getBlogImagePath()): ?>
								<img style="width:15em;" class='<?= $singleBlogItemArray->getBlogImageAlignment() ?>' src='<?= $singleBlogItemArray->getBlogImagePath() ?>' alt='' title=''>
							<?php endif ?>
							
							<br>
							<br>
							<?= nl2br( $singleBlogItemArray->getBlogContent() ) ?>
						</p>
						
						<div class='clearer'></div>
						
						<br>
						<br>
						<hr>
						<br>
						
						
					</article>
					
				<?php endforeach ?>
			<?php endif ?>
			
		</main>
	
		<!-- ---------- BLOG ENTRIES END ---------- -->
		
		
		
		<!-- ---------- CATEGORY FILTER LINKS START ---------- -->
		
		<nav class="categories fright" style = "width: 25%; padding: 50px 0; border: 3px solid lightgray; border-radius: 10px; text-align: center;">

			<?php if( !$allCategoriesArray ): ?>
				<p class="info">Noch keine Kategorien vorhanden.</p>
			
			<?php else: ?>
		
				<?php foreach( $allCategoriesArray AS $categorySingleItemArray ): ?>
					<p><a href="?action=filterByCategory&catID=<?=$categorySingleItemArray->getCatID()?>" <?php if( $categorySingleItemArray->getCatID() == $categoryFilterId ) echo 'class="active"' ?> > <?= $categorySingleItemArray->getCatLabel() ?></a></p>
				<?php endforeach ?>

			<?php endif ?> 
		</nav>

		<div class="clearer"></div>


		<!-- ---------- CATEGORY FILTER LINKS END ---------- -->
		
	</body>

</html>







