<?php
#*******************************************************************************************#


				#****************************************#
				#************ CLASS CATEGORY ************#
				#****************************************#

				/*
					Die Klasse ist quasi der Bauplan/die Vorlage für alle Objekte, die aus ihr erstellt werden.
					Sie gibt die Eigenschaften/Attribute eines späteren Objekts vor (Variablen) sowie 
					die "Fähigkeiten" (Methoden/Funktionen), über die das spätere Objekt besitzt.

					Jedes Objekt einer Klasse ist nach dem gleichen Schema aufgebaut (gleiche Eigenschaften und Methoden), 
					besitzt aber i.d.R. unterschiedliche Attributswerte.
				*/

				
#*******************************************************************************************#


				/**
				*
				*	Class represents a blog category
				*	Adding new category
				*	Read all category
				*	Must be embedded in a Blog object
				*
				*/

				class Category {
					
					#*******************************#
					#********** ATTRIBUTE **********#
					#*******************************#
					
					/* 
						Innerhalb der Klassendefinition müssen Attribute nicht zwingend initialisiert werden.
						Ein Weglassen der Initialisierung bewirkt das gleiche, wie eine Initialisierung mit NULL.
					*/

					private $catID;
					private $catLabel;
					

					
					#***********************************************************#
					
					
					#*********************************#
					#********** CONSTRUCTOR **********#
					#*********************************#
					
					/*
						Der Constructor ist eine magische Methode und wird automatisch aufgerufen,
						sobald mittels des new-Befehls ein neues Objekt erstellt wird.
						Der Constructor erstellt eine neue Klasseninstanz/Objekt.
						Soll ein Objekt beim Erstellen bereits mit Attributwerten versehen werden,
						muss ein eigener Constructor geschrieben werden. Dieser nimmt die Werte in 
						Form von Parametern (genau wie bei Funktionen) entgegen und ruft seinerseits 
						die entsprechenden Setter auf, um die Werte zuzuweisen.					
					*/
					
					/**
					*
					*	@construct
					*	Creates a Category object
					*	
					*	@param	String		$catLabel=NULL			Category name
					*	@param	Int|String	$catID=NULL				Category ID given by the database
					*	
					*	@return	Void
					*
					*/
					
					public function __construct( $catLabel=NULL, $catID=NULL ) {
if(DEBUG_CC)	echo "<h3 class='debugClass'>🛠 <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "()  (<i>" . basename(__FILE__) . "</i>)</h3>\n";						
					
					// Setter nur aufrufen, wenn der jeweilige Parameter keinen Leerstring und nicht NULL enthält
					if( $catID 	!== '' AND $catID 		!== NULL )	$this->setCatID($catID);
					if( $catLabel !== '' AND $catLabel 	!== NULL )	$this->setCatLabel($catLabel);
/*					
if(DEBUG_CC)	echo "<pre class='debugClass value'><b>Line " . __LINE__ .  "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_CC)	print_r($this);					
if(DEBUG_CC)	echo "</pre>";	
*/
				}
					
					
					
					#********** DESTRUCTOR **********#
					/*
						Der Destructor ist eine magische Methode und wird automatisch aufgerufen,
						sobald ein Objekt mittels unset() gelöscht wird, oder sobald das Skript beendet ist.
						Der Destructor gibt den vom gelöschten Objekt belegten Speicherplatz wieder frei.
					*/
					
					/**
					*
					*	@destruct
					*
					*	@return	Void
					*
					*/
					
					public function __destruct() {
if(DEBUG_CC)	echo "<h3 class='debugClass'>☠️  <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "()  (<i>" . basename(__FILE__) . "</i>)</h3>\n";						
				}					
					
					
					#***********************************************************#

					
					#*************************************#
					#********** GETTER & SETTER **********#
					#*************************************#
				
					#********** CATEGORY ID **********#
					public function getCatID() : NULL|int|string {
						return $this->catID;
					}
					public function setCatID(int|string $value) : void {
						$this->catID = cleanString($value);
						
						#********** VALIDATE DATA TYPE CONFORMITY **********#
						if( filter_var($this->getCatID(), FILTER_VALIDATE_INT) !== false ) {
							$this->catID = intval( $this->getCatID() );
						}
					}
					
					#********** CATEGORY LABEL **********#
					public function getCatLabel() : NULL|string {
						return $this->catLabel;
					}
					public function setCatLabel(NULL|string $value) : void {
						$this->catLabel = cleanString($value);
					}

					
					#***********************************************************#
					

					#******************************#
					#********** METHODEN **********#
					#******************************#
					
					#********** CHECK IF CATEGORY EXISTS IN DB **********#
					/**
					*
					*	Checks if category exists in database from user input
					*
					*	@param	PDO								$PDO		DB connection via PDO
					*
					*	@return	Int								Number of matching DB entries
					*
					*/
					
					public static function checkIfCategoryExistsInDB($PDO){
						$sql 		= 'SELECT COUNT(catLabel) FROM category 
										WHERE catLabel = ?';
										
						$params 	= array( $this->getCatLabel() );
						
						// Schritt 2 DB: SQL-Statement vorbereiten
						$PDOStatement = $PDO->prepare($sql);
						
						// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
						try {	
							$PDOStatement->execute($params);						
						} catch(PDOException $error) {
if(DEBUG_C)				echo "<p class='debugClass err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
							$dbError = 'Fehler beim Zugriff auf die Datenbank!';
						}
						
						// Schritt 4 DB: Daten weiterverarbeiten
						/*
							Bei SELECT COUNT(): Rückgabewert von COUNT() über $PDOStatement->fetchColumn() 
							auslesen
						*/
						$count = $PDOStatement->fetchColumn();
if(DEBUG_C)			echo "<p class='debugClass value'><b>Line " . __LINE__ . "</b>: \$count: $count <i>(" . basename(__FILE__) . ")</i></p>\n";
						
						// Schritt 5 DB: Datenbankverbindung wieder schließen
						DBClose();
						
						return $count;
					}

					#***********************************************************#
				
					#********** SAVE TO DB **********#
					
					/**
					*
					*	Saves new data set of calling object's attributes data into database
					*	Writes the database's last insert id on success into calling object
					*
					*	@param	PDO	$PDO		DB connection via PDO
					*
					*	@return	Boolean			true on success | false on error					
					*
					*/
					
					
					
					public static function saveToDb(PDO $PDO) {
if(DEBUG_C)			echo "<h3 class='debugClass'>🌀 <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "() (<i>" . basename(__FILE__) . "</i>)</h3>\n";
						
						$sql 		= 'INSERT INTO category (catLabel) VALUES (?)';
					
						$params 	= array( $this->getCatLabel());
						
if(DEBUG_CC)			echo "<pre class='debugClass value'><b>Line " . __LINE__ .  "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_CC)			print_r($params);					
if(DEBUG_CC)			echo "</pre>";

						// Schritt 2 DB: SQL-Statement vorbereiten
						$PDOStatement = $PDO->prepare($sql);
						
						// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
						try {	
							$PDOStatement->execute($params);						
						} catch(PDOException $error) {
if(DEBUG_C)				echo "<p class='debugClass err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
							$dbError = 'Fehler beim Zugriff auf die Datenbank!';
						}
						
						// Schritt 4 DB: Daten weiterverarbeiten

						$rowCount = $PDOStatement->rowCount();
if(DEBUG_C)			echo "<p class='debugClass value'><b>Line " . __LINE__ . "</b>: \$rowCount: $rowCount <i>(" . basename(__FILE__) . ")</i></p>\n";

						if( $rowCount !== 1 ) {
							// Fehlerfall
							// Schritt 5 DB: Datenbankverbindung wieder schließen
							DBClose();
							return false;
							
						} else {
							// Erfolgsfall

							$this->setCatID( $PDO->lastInsertId() );
							

							// Schritt 5 DB: Datenbankverbindung wieder schließen
							DBClose();
							
							return true;
						}
						
						
					}
					#***********************************************************#
					
					#********** FETCH ALL FROM DB **********#
					
					/**
					*
					*	Fetches all blog object's data from DB 
					*
					*	@param	PDO		$PDO		DB connection via PDO
					*
					*	@return	Array					All data set array
					*
					*/
					public static function fetchAllFromDB(PDO $PDO){
if(DEBUG_C)			echo "<h3 class='debugClass'>🌀 <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "() (<i>" . basename(__FILE__) . "</i>)</h3>\n";

						$sql 		= 'SELECT * FROM category';
						
						$params 	= array();
						
						// Schritt 2 DB: SQL-Statement vorbereiten
						$PDOStatement = $PDO->prepare($sql);
						
						// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
						try {	
							$PDOStatement->execute($params);						
						} catch(PDOException $error) {
if(DEBUG_C)				echo "<p class='debugClass err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
							$dbError = 'Fehler beim Zugriff auf die Datenbank!';
						}
						//$dataSet = $PDOStatement->fetch(PDO::FETCH_ASSOC);
						// Schritt 4 DB: Daten weiterverarbeiten
						
						while( $dataSet = $PDOStatement->fetch(PDO::FETCH_ASSOC) ) {
							$allCategoriesArray[$dataSet['catID']] = new Category( $dataSet['catLabel'], $dataSet['catID']);
						}
/*						
if(DEBUG_C)			echo "<pre class='debugClass value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_C)			print_r($allCategoriesArray);					
if(DEBUG_C)			echo "</pre>";
*/
						// Schritt 5 DB: Datenbankverbindung wieder schließen
						DBClose();

					return $allCategoriesArray;

					}

					#***********************************************************#
					
				}
				
				
#*******************************************************************************************#
?>


















