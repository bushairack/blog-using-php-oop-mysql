<?php
#*******************************************************************************************#


				#****************************************#
				#************ CLASS USER ****************#
				#****************************************#

				/*
					Die Klasse ist quasi der Bauplan/die Vorlage für alle Objekte, die aus ihr erstellt werden.
					Sie gibt die Eigenschaften/Attribute eines späteren Objekts vor (Variablen) sowie 
					die "Fähigkeiten" (Methoden/Funktionen), über die das spätere Objekt besitzt.

					Jedes Objekt einer Klasse ist nach dem gleichen Schema aufgebaut (gleiche Eigenschaften und Methoden), 
					besitzt aber i.d.R. unterschiedliche Attributswerte.
				*/

				
#*******************************************************************************************#


				/**
				*
				*	Class represents a user
				*	Must be embedded in an Blog object
				*
				*/

				class User {
					
					#*******************************#
					#********** ATTRIBUTE **********#
					#*******************************#
					
					/* 
						Innerhalb der Klassendefinition müssen Attribute nicht zwingend initialisiert werden.
						Ein Weglassen der Initialisierung bewirkt das gleiche, wie eine Initialisierung mit NULL.
					*/

					private $userID;
					private $userFirstName;
					private $userLastName;
					private $userEmail;
					private $userCity;
					private $userPassword;
					

					
					#***********************************************************#
					
					
					#*********************************#
					#********** CONSTRUCTOR **********#
					#*********************************#
					
					/*
						Der Constructor ist eine magische Methode und wird automatisch aufgerufen,
						sobald mittels des new-Befehls ein neues Objekt erstellt wird.
						Der Constructor erstellt eine neue Klasseninstanz/Objekt.
						Soll ein Objekt beim Erstellen bereits mit Attributwerten versehen werden,
						muss ein eigener Constructor geschrieben werden. Dieser nimmt die Werte in 
						Form von Parametern (genau wie bei Funktionen) entgegen und ruft seinerseits 
						die entsprechenden Setter auf, um die Werte zuzuweisen.					
					*/
					
					/**
					*
					*	@construct
					*	Creates a user object
					*	
					*	@param	Int|String	$userID=NULL					Record ID given by the database
					*	@param	String		$userFirstName=NULL			first name of user from database
					*	@param	String		$userLastName=NULL			last name of user from database
					*	@param	String		$userEmail=NULL				email of user																																																				
					*	@param	String		$userCity=NULL					city of user from database
					*	@param	String		$userPassword=NULL			password of user
					*	
					*	@return	Void
					*
					*/
					
					public function __construct( $userID = NULL, $userFirstName = NULL, $userLastName = NULL, $userEmail = NULL, $userCity = NULL, $userPassword = NULL ) {
if(DEBUG_CC)	echo "<h3 class='debugClass'>🛠 <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "()  (<i>" . basename(__FILE__) . "</i>)</h3>\n";						

					// Setter nur aufrufen, wenn der jeweilige Parameter keinen Leerstring und nicht NULL enthält
					if( $userID 				!== '' AND $userID 				!== NULL )	$this->setUserID($userID);
					if( $userFirstName 		!== '' AND $userFirstName 		!== NULL )	$this->setUserFirstName($userFirstName);
					if( $userLastName 		!== '' AND $userLastName 		!== NULL )	$this->setUserLastName($userLastName);
					if( $userEmail 			!== '' AND $userEmail 			!== NULL )	$this->setUserEmail($userEmail);
					if( $userCity 				!== '' AND $userCity 			!== NULL )	$this->setUserCity($userCity);
					if( $userPassword 		!== '' AND $userPassword 		!== NULL )	$this->setUserPassword($userPassword);

/*
if(DEBUG_CC)	echo "<pre class='debugClass value'><b>Line " . __LINE__ .  "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_CC)	print_r($this);					
if(DEBUG_CC)	echo "</pre>";	
*/
				}
					
					
					
					#********** DESTRUCTOR **********#
					/*
						Der Destructor ist eine magische Methode und wird automatisch aufgerufen,
						sobald ein Objekt mittels unset() gelöscht wird, oder sobald das Skript beendet ist.
						Der Destructor gibt den vom gelöschten Objekt belegten Speicherplatz wieder frei.
					*/
					
					/**
					*
					*	@destruct
					*
					*	@return	Void
					*
					*/
					public function __destruct() {
if(DEBUG_CC)	echo "<h3 class='debugClass'>☠️  <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "()  (<i>" . basename(__FILE__) . "</i>)</h3>\n";						
				}					
					
					
					#***********************************************************#

					
					#*************************************#
					#********** GETTER & SETTER **********#
					#*************************************#
				
					#********** USER ID **********#
					public function getUserID() : NULL|int|string {
						return $this->userID;
					}
					public function setUserID(int|string $value) : void {
						$this->userID = cleanString($value);
						
						#********** VALIDATE DATA TYPE CONFORMITY **********#
						if( filter_var($this->getUserID(), FILTER_VALIDATE_INT) !== false ) {
							$this->userID = intval( $this->getUserID() );	
						}
					}
					
					
					#********** USER FIRST NAME **********#
					public function getUserFirstName() : NULL|string {
						return $this->userFirstName;
					}
					public function setUserFirstName(NULL|string $value) : void {
						$this->userFirstName = cleanString($value);
					}
					
					
					#********** USER LAST NAME **********#
					public function getUserLastName() : NULL|string {
						return $this->userLastName;
					}
					public function setUserLastName(NULL|string $value) : void {
						$this->userLastName = cleanString($value);
					}
					
					#********** USER EMAIL **********#
					public function getUserEmail() : NULL|string {
						return $this->userEmail;
					}
					public function setUserEmail(NULL|string $value) : void {
						$this->userEmail = cleanString($value);
					}
					#********** USER CITY **********#
					public function getUserCity() : NULL|string {
						return $this->userCity;
					}
					public function setUserCity(NULL|string $value) : void {
						$this->userCity = cleanString($value);
					}
					
					#********** USER PASSWORD **********#
					public function getUserPassword() : NULL|string {
						return $this->userPassword;
					}
					public function setUserPassword(NULL|string $value) : void {
						$this->userPassword = cleanString($value);
					}
					
					#********** VIRTUAL ATTRIBUTES **********#
					public function getFullName() : NULL|string {
						return $this->getUserFirstName() . ' ' . $this->getUserLastName();
					}
					
					
					
					
					
					#***********************************************************#
					

					#******************************#
					#********** METHODEN **********#
					#******************************#

					#********** FETCH FROM DB **********#
					
					/**
					*
					*	Fetches calling user object's data from DB using userEmail
					*	If corresponding data set was found: writes data into calling object
					*	Calling blog object must contain User object
					*
					*	@param	PDO		$PDO		DB connection via PDO
					*
					*	@return	Array					All data set array
					*
					*/
				
					public static function fetchFromDB( PDO $PDO, $userEmail ) {
if(DEBUG_C)			echo "<h3 class='debugClass'>🌀 <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "() (<i>" . basename(__FILE__) . "</i>)</h3>\n";


						#********** FETCH USER DATA FROM DB BY USER EMAIL **********#	
						
						$sql 		= 	'SELECT userID, userFirstName, userLastName, userEmail, userPassword FROM user 
										 WHERE userEmail = ?';
				 
						
						$params 	= array( $userEmail );
						
						// Schritt 2 DB: SQL-Statement vorbereiten
						$PDOStatement = $PDO->prepare($sql);
					
						// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
						try {	
							$PDOStatement->execute($params);						
						} catch(PDOException $error) {
if(DEBUG_C)				echo "<p class='debugClass err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
							$dbError = 'Fehler beim Zugriff auf die Datenbank!';
						}
						
						// Schritt 4 DB: Daten weiterverarbeiten
						$dataSet = $PDOStatement->fetch(PDO::FETCH_ASSOC);
						
if(DEBUG_C)			echo "<pre class='debugClass value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_C)			print_r($dataSet);					
if(DEBUG_C)			echo "</pre>";

						// Schritt 5 DB: Datenbankverbindung wieder schließen
						DBClose();

						return $dataSet;
					
					
						#***********************************************************#
						
					}	
				}
				
				
#*******************************************************************************************#
?>


















