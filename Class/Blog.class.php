<?php
#*******************************************************************************************#


				#****************************************#
				#************** CLASS BLOG **************#
				#****************************************#

				/*
					Die Klasse ist quasi der Bauplan/die Vorlage für alle Objekte, die aus ihr erstellt werden.
					Sie gibt die Eigenschaften/Attribute eines späteren Objekts vor (Variablen) sowie 
					die "Fähigkeiten" (Methoden/Funktionen), über die das spätere Objekt besitzt.

					Jedes Objekt einer Klasse ist nach dem gleichen Schema aufgebaut (gleiche Eigenschaften und Methoden), 
					besitzt aber i.d.R. unterschiedliche Attributswerte.
				*/

				
#*******************************************************************************************#

				/**
				*
				*	Class represents a Blog
				*	Must embed Category object and User object
				*
				*/


				class Blog {
					
					#*******************************#
					#********** ATTRIBUTE **********#
					#*******************************#
					
					/* 
						Innerhalb der Klassendefinition müssen Attribute nicht zwingend initialisiert werden.
						Ein Weglassen der Initialisierung bewirkt das gleiche, wie eine Initialisierung mit NULL.
					*/
					
						private $blogID;
						private $blogHeadline;
						private $blogImagePath;
						private $blogImageAlignment;
						private $blogContent;
						private $blogDate;
						
						
						#********** CATEGORY OBJECT **********#
						private $Category;
						
						
						#********** USER OBJECT **********#
						private $User;
					
					

					
					#***********************************************************#
					
					
					#*********************************#
					#********** CONSTRUCTOR **********#
					#*********************************#
					
					/*
						Der Constructor ist eine magische Methode und wird automatisch aufgerufen,
						sobald mittels des new-Befehls ein neues Objekt erstellt wird.
						Der Constructor erstellt eine neue Klasseninstanz/Objekt.
						Soll ein Objekt beim Erstellen bereits mit Attributwerten versehen werden,
						muss ein eigener Constructor geschrieben werden. Dieser nimmt die Werte in 
						Form von Parametern (genau wie bei Funktionen) entgegen und ruft seinerseits 
						die entsprechenden Setter auf, um die Werte zuzuweisen.					
					*/
					
					/**
					*
					*	@construct
					*	Creates a blog object
					*	Must embed (empty or filled) Category and User object
					*
					*	@param	User 			$User								User object corresponding to blog object
					*	@param	Category 	$Category						Category object defining Category of blog
					*	@param	String 		$blogHeadline=NULL			Blog Headline
					*	@param	String 		$blogImagePath=NULL			Blog image path, if image present
					*	@param	String 		$blogImageAlignment=NULL	Blog image alignment
					*	@param	String 		$blogContent=NULL				Blog content text
					*	@param	String 		$blogDate=NULL					Blog stored date from database
					*	@param	Int|String 	$blogID=NULL					Record ID given by database
					*
					*	@return	Void
					*
					*/
					
					public function __construct(	$User, $Category,
															$blogHeadline=NULL, $blogImagePath=NULL, $blogImageAlignment=NULL, 
															$blogContent=NULL, $blogDate=NULL, $blogID=NULL ) {
if(DEBUG_CC)	echo "<h3 class='debugClass'>🛠 <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "()  (<i>" . basename(__FILE__) . "</i>)</h3>\n";						

					// Die einzubettenden Objekte müssen immer vorhanden sein
					$this->setUser($User);
					$this->setCategory($Category);
					
					// Setter nur aufrufen, wenn der jeweilige Parameter keinen Leerstring und nicht NULL enthält
					if( $blogHeadline 			!== '' AND $blogHeadline 			!== NULL )	$this->setBlogHeadline($blogHeadline);
					if( $blogImagePath 			!== '' AND $blogImagePath 			!== NULL )	$this->setBlogImagePath($blogImagePath);
					if( $blogImageAlignment 	!== '' AND $blogImageAlignment 	!== NULL )	$this->setBlogImageAlignment($blogImageAlignment);
					if( $blogContent 				!== '' AND $blogContent 			!== NULL )	$this->setBlogContent($blogContent);
					if( $blogDate 					!== '' AND $blogDate 				!== NULL )	$this->setBlogDate($blogDate);
					if( $blogID 					!== '' AND $blogID 					!== NULL )	$this->setBlogID($blogID);
/*						
if(DEBUG_CC)	echo "<pre class='debugClass value'><b>Line " . __LINE__ .  "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_CC)	print_r($this);					
if(DEBUG_CC)	echo "</pre>";	
*/				}				
					
					
					
					#********** DESTRUCTOR **********#
					/*
						Der Destructor ist eine magische Methode und wird automatisch aufgerufen,
						sobald ein Objekt mittels unset() gelöscht wird, oder sobald das Skript beendet ist.
						Der Destructor gibt den vom gelöschten Objekt belegten Speicherplatz wieder frei.
					*/
					
					/**
					*
					*	@destruct
					*
					*	@return	Void
					*
					*/
					public function __destruct() {
if(DEBUG_CC)	echo "<h3 class='debugClass'>☠️  <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "()  (<i>" . basename(__FILE__) . "</i>)</h3>\n";						
				}					
					
					
					#***********************************************************#

					
					#*************************************#
					#********** GETTER & SETTER **********#
					#*************************************#
				
					#********** BLOG ID **********#
					public function getBlogID() : NULL|int|string {
						return $this->blogID;
					}
					public function setBlogID(int|string $value) : void {
						$this->blogID = cleanString($value);
						
						#********** VALIDATE DATA TYPE CONFORMITY **********#
						if( filter_var($this->getBlogID(), FILTER_VALIDATE_INT) !== false ) {
							$this->blogID = intval( $this->getBlogID() );
						}
					}
					
					
					#********** BLOG HEADLINE **********#
					public function getBlogHeadline() : NULL|string {
						return $this->blogHeadline;
					}
					public function setBlogHeadline(NULL|string $value) : void {
						$this->blogHeadline = cleanString($value);
					}
					
					#********** BLOG IMAGE PATH **********#
					public function getBlogImagePath() : NULL|string {
						return $this->blogImagePath;
					}
					public function setBlogImagePath(NULL|string $value) : void {
						$this->blogImagePath = cleanString($value);
					}
					
					#********** BLOG IMAGE ALIGNMENT **********#
					public function getBlogImageAlignment() : NULL|string {
						return $this->blogImageAlignment;
					}
					public function setBlogImageAlignment(NULL|string $value) : void {
						$this->blogImageAlignment = cleanString($value);
					}
					
					#********** BLOG CONTENT **********#
					public function getBlogContent() : NULL|string {
						return $this->blogContent;
					}
					public function setBlogContent(NULL|string $value) : void {
						$this->blogContent = cleanString($value);
					}
					
					#********** BLOG DATE **********#
					public function getBlogDate() : NULL|string {
						return $this->blogDate;
					}
					public function setBlogDate(NULL|string $value) : void {
						$this->blogDate = cleanString($value);
					}
					
					#********** CATEGORY OBJECT **********#
					public function getCategory() : Category {
						return $this->Category;
					}
					public function setCategory(Category $value) : void {
						$this->Category = $value;
					}
					
					#********** USER OBJECT **********#
					public function getUser() : User {
						return $this->User;
					}
					public function setUser(User $value) : void {
						$this->User = $value;
					}
					
					
					#***********************************************************#
					

					#******************************#
					#********** METHODEN **********#
					#******************************#


					#********** SAVE TO DB **********#
					/**
					*
					*	Saves new data set of calling object's attributes values into database
					*	Uses embedded user object's userID as foreign key
					*	Writes the database's last insert id on success into calling object
					*
					*	@param	PDO	$PDO		DB connection via PDO
					*
					*	@return	Boolean			true on success | false on error					
					*
					*/
					public static function saveToDB(PDO $PDO, $userID) {
if(DEBUG_C)			echo "<h3 class='debugClass'>🌀 <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "() (<i>" . basename(__FILE__) . "</i>)</h3>\n";
						
						$sql 		= 	'INSERT INTO blog (blogHeadline, blogImagePath, blogImageAlignment, blogContent, catID, userID)
											 VALUES (?, ?, ?, ?, ?, ?) ';
											 
						$params 	= array(	$this->getBlogHeadline(),
												$this->getBlogImagePath(),
												$this->getBlogImageAlignment(),
												$this->getBlogContent(),
												$this->getCategory()->getCatID(),
												$userID );
												
if(DEBUG_CC)		echo "<pre class='debugClass value'><b>Line " . __LINE__ .  "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_CC)		print_r($params);					
if(DEBUG_CC)		echo "</pre>";

						// Schritt 2 DB: SQL-Statement vorbereiten
						$PDOStatement = $PDO->prepare($sql);
						
						// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
						try {	
							$PDOStatement->execute($params);						
						} catch(PDOException $error) {
if(DEBUG_C)				echo "<p class='debugClass err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
							$dbError = 'Fehler beim Zugriff auf die Datenbank!';
						}
						
						// Schritt 4 DB: Daten weiterverarbeiten

						$rowCount = $PDOStatement->rowCount();
if(DEBUG_C)			echo "<p class='debugClass value'><b>Line " . __LINE__ . "</b>: \$rowCount: $rowCount <i>(" . basename(__FILE__) . ")</i></p>\n";
						
						if( $rowCount !== 1 ) {
							// Fehlerfall
							// Schritt 5 DB: Datenbankverbindung wieder schließen
							DBClose();
							return false;
							
						} else {
							// Erfolgsfall

							$this->setBlogID( $PDO->lastInsertId() );
							

							// Schritt 5 DB: Datenbankverbindung wieder schließen
							DBClose();
							
							return true;
						}		
					}


					
					#***********************************************************#
					
					#********** FETCH ALL FROM DB **********#
					
					/**
					*
					*	Fetches calling blog object's data and embedded object's data from DB 
					*	by either user id and category id via INNER JOIN
					*	If corresponding data set was found: writes data into calling object and 
					*	embedded objects
					*	Calling blog object must contain User object and Category object
					* Fetch all datas with the help of while loop
					*
					*	@param	PDO		$PDO		DB connection via PDO
					*
					*	@return	Array					All data set array
					*
					*/
					
					public static function fetchAllFromDB(PDO $PDO, $sql, $params){
if(DEBUG_C)			echo "<h3 class='debugClass'>🌀 <b>Line " . __LINE__ .  "</b>: Aufruf " . __METHOD__ . "() (<i>" . basename(__FILE__) . "</i>)</h3>\n";
						
						
						// Schritt 2 DB: SQL-Statement vorbereiten
						$PDOStatement = $PDO->prepare($sql);
				
						// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
						try {	
							$PDOStatement->execute($params);								
						} catch(PDOException $error) {
if(DEBUG)			echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
						$dbError = 'Fehler beim Zugriff auf die Datenbank!';
					}
					
					//$dataSet = $PDOStatement->fetchAll(PDO::FETCH_ASSOC);
					
					//$User, $Category, $blogHeadline=NULL, $blogImagePath=NULL, $blogImageAlignment=NULL, 
					//$blogContent=NULL, $blogDate=NULL, $blogID=NULL
					$allBlogArticlesArray = NULL;
					while( $dataSet = $PDOStatement->fetch(PDO::FETCH_ASSOC) ) {
							$allBlogArticlesArray[$dataSet['blogID']] = new Blog(	 new User( $dataSet['userID'], $dataSet['userFirstName'], $dataSet['userLastName'], $dataSet['userEmail'], $dataSet['userCity'] ),
																									 new Category( $dataSet['catLabel'], $dataSet['catID']), 
																									 $dataSet['blogHeadline'],
																									 $dataSet['blogImagePath'],
																									 $dataSet['blogImageAlignment'],
																									 $dataSet['blogContent'],
																									 $dataSet['blogDate'],
																									 $dataSet['blogID']);
						}
					
					
					
					
					
					
					
					
					// Gefundene Datensätze für spätere Verarbeitung in Zweidimensionales Array zwischenspeichern
					//$allBlogArticlesArray = $PDOStatement->fetchAll(PDO::FETCH_ASSOC);
				
					// DB-Verbindung schließen
					DBClose($PDO);
					
/*
if(DEBUG_V)		echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)		print_r($allBlogArticlesArray);					
if(DEBUG_V)		echo "</pre>";
*/
					return $allBlogArticlesArray;

				}
				
			}	
#*******************************************************************************************#
?>


















