<?php
#***************************************************************************************#


				/**
				*
				*	@file 				Start Page with Login credentials
				*	@author 				Bushaira Chakkikalathil <abcd@ab.de>
				*	@copyright			Project-PHP - mySQL - Datenbankanbindung
				*	@lastModifyDate	2022-05-11
				*	
				*/


#***************************************************************************************#


				#***********************************#
				#********** CONFIGURATION **********#
				#***********************************#
				
				require_once('include/config.inc.php');
				require_once('include/db.inc.php');
				require_once('include/form.inc.php');
				include_once('include/dateTime.inc.php');
				include_once('include/authentification.inc.php');
				
				
				#********** INCLUDE CLASSES **********#
				
				require_once('Class/User.class.php');
				require_once('Class/Category.class.php');
				require_once('Class/Blog.class.php');


#***************************************************************************************#


#***************************************************************************************#

			
				#******************************************#
				#********** VALIDATE PAGE ACCESS **********#
				#******************************************#
				
				$userID = securePageAccess('blogProjectOOP');
						
				// fetch more user data from session
				
				
				$userFirstName = $_SESSION['userFirstName'];
				$userLastName	= $_SESSION['userLastName'];

			
#***************************************************************************************#

				$BlogObject = new Blog(new User(), new Category() );
				
				$BlogObject->getUser()->setUserFirstName( $userFirstName );
				$BlogObject->getUser()->setUserLastName( $userLastName );
				$BlogObject->getUser()->setUserID( $userID  );

#***************************************************************************************#	

			
				#******************************************#
				#********** INITIALIZE VARIABLES **********#
				#******************************************#
				
				$catID 						= NULL;
				$blogHeadline 				= NULL;
				$blogContent 				= NULL;
				$blogImageAlignment 		= NULL;
				$catLabel 					= NULL;
				$blogImagePath 			= NULL;
				$allCategoriesArray  	= NULL;
				
				$errorCatLabel				= NULL;
				$errorHeadline 			= NULL;
				$errorImageUpload 		= NULL;
				$errorContent 				= NULL;
				
				$dbError						= NULL;
				$dbSuccess					= NULL;


#***************************************************************************************#

	
				#********************************************#
				#********** PROCESS URL PARAMETERS **********#
				#********************************************#
				
				// Schritt 1 URL: Prüfen, ob Parameter übergeben wurde
				if( isset($_GET['action']) ) {
if(DEBUG)		echo "<p class='debug'>🧻 Line <b>" . __LINE__ . "</b>: URL-Parameter 'action' wurde übergeben... <i>(" . basename(__FILE__) . ")</i></p>";	
			
					// Schritt 2 URL: Werte auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte werden ausgelesen und entschärft... <i>(" . basename(__FILE__) . ")</i></p>\n";
					$action = cleanString($_GET['action']);
if(DEBUG_V)		echo "<p class='debug value'>Line <b>" . __LINE__ . "</b>: \$action = $action <i>(" . basename(__FILE__) . ")</i></p>";
		
					// Schritt 3 URL: ggf. Verzweigung
					
					
					#********** LOGOUT **********#
					if( $_GET['action'] === 'logout' ) {
if(DEBUG)			echo "<p class='debug'>📑 Line <b>" . __LINE__ . "</b>: 'Logout' wird durchgeführt... <i>(" . basename(__FILE__) . ")</i></p>";	
						
						logout();
					}
					
				} // PROCESS URL PARAMETERS END



#***************************************************************************************#	

				#*************************************************#
				#********** PROCESS FORM 'NEW CATEGORY' **********#
				#*************************************************#
				
				// Schritt 1 FORM: Prüfen, ob Formular abgeschickt wurde
				if( isset($_POST['formNewCategory']) ) {
if(DEBUG)		echo "<p class='debug'>🧻 Line <b>" . __LINE__ . "</b>: Formular 'New Category' wurde abgeschickt... <i>(" . basename(__FILE__) . ")</i></p>";	

/*
if(DEBUG_V)		echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)		print_r($_POST);					
if(DEBUG_V)		echo "</pre>";
*/
					// Schritt 2 FORM: Werte auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte werden ausgelesen und entschärft... <i>(" . basename(__FILE__) . ")</i></p>\n";
					
					cleanString($BlogObject->getCategory()->setCatLabel(	$_POST['catLabel']));

					$catLabel = cleanString($BlogObject->getCategory()->getCatLabel());
if(DEBUG_V)		echo "<p class='debug value'>Line <b>" . __LINE__ . "</b>: \$catLabel: $catLabel <i>(" . basename(__FILE__) . ")</i></p>";
				
					// Schritt 3 FORM: Werte ggf. validieren
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Feldwerte werden validiert... <i>(" . basename(__FILE__) . ")</i></p>\n";
					$errorCatLabel = checkInputString($catLabel);
					
					
					#********** FINAL FORM VALIDATION **********#
					if( $errorCatLabel ) {
if(DEBUG)			echo "<p class='debug err'>Line <b>" . __LINE__ . "</b>: Das Formular enthält noch Fehler! <i>(" . basename(__FILE__) . ")</i></p>";						
						
					} else {
if(DEBUG)			echo "<p class='debug ok'>Line <b>" . __LINE__ . "</b>: Das Formular ist formal fehlerfrei. <i>(" . basename(__FILE__) . ")</i></p>";						
						
						// Schritt 4 FORM: Daten weiterverarbeiten
						
						#********** CHECK IF CATEGORY NAME ALREADY EXISTS **********#
						
if(DEBUG)			echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Prüfen, ob catLabel bereits von einem anderen Customer registriert wurde... <i>(" . basename(__FILE__) . ")</i></p>\n";

						// DB-Verbindung herstellen
						$PDO = DBConnect('bloog_oop');
						
						if( $BlogObject->getCategory()->checkIfCategoryExistsInDB($PDO) > 0 ) {
							// Fehlerfall
if(DEBUG)				echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: Die Category '{$BlogObject->getCategory()->getCatLabel()}' ist bereits existiert! <i>(" . basename(__FILE__) . ")</i></p>\n";				
							// Fehlermeldung an User
							$errorCatLabel = 'Es existiert bereits!';							
							
							// DB-Verbindung schließen
							DBClose();
						} else {
							
							// Erfolgsfall
if(DEBUG)				echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Die Category '{$BlogObject->getCategory()->getCatLabel()}' ist nicht existiert. <i>(" . basename(__FILE__) . ")</i></p>\n";				
							$dbSuccess = "Die neue Kategorie mit dem Namen <b>'{$BlogObject->getCategory()->getCatLabel()}'</b> wurde erfolgreich gespeichert.";
							

							// DB-Verbindung schließen
							DBClose();

						
							#********** SAVE CATEGORY INTO DATABASE **********#
if(DEBUG)				echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Speichere Customerdaten in die DB... <i>(" . basename(__FILE__) . ")</i></p>\n";

							// DB-Verbindung herstellen
							$PDO=DBConnect('bloog_oop');
							
if(DEBUG)				echo "<p class='debug'><b>Line " . __LINE__ . "</b>: Speichere New Category... <i>(" . basename(__FILE__) . ")</i></p>\n";

							if( $BlogObject->getCategory()->saveToDb($PDO) === false ) {
									// Fehlerfall
if(DEBUG)						echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER beim Speichern des Customerdatensatzes! <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
									// Fehlermeldung an User
									$dbError = 'Es ist ein Fehler aufgetreten! Bitte versuchen Sie es später noch einmal.';
									
									// DB-Verbindung schließen
									DBClose();	
							} else {
									// Erfolgsfall
if(DEBUG)						echo "<p class='debug ok'>Line <b>" . __LINE__ . "</b>: Kategorie <b>'{$BlogObject->getCategory()->getCatLabel()}'</b> wurde erfolgreich unter der ID'{$BlogObject->getCategory()->getCatID()}' in der DB gespeichert. <i>(" . basename(__FILE__) . ")</i></p>";								
									
									$dbSuccess = "Die neue Kategorie mit dem Namen <b>'{$BlogObject->getCategory()->getCatLabel()}'</b> wurde erfolgreich gespeichert.";

									
									// DB-Verbindung schließen
									DBClose();
							}								
	
						}
					}
				}
	
#***************************************************************************************#

				#***************************************************#
				#********** PROCESS FORM 'NEW BLOG ENTRY' **********#
				#***************************************************#
				
				// Schritt 1 FORM: Prüfen, ob Formular abgeschickt wurde
				if( isset($_POST['formNewBlogEntry']) ) {			
if(DEBUG)		echo "<p class='debug'>🧻 Line <b>" . __LINE__ . "</b>: Formular 'New Blog Entry' wurde abgeschickt... <i>(" . basename(__FILE__) . ")</i></p>";	

if(DEBUG_V)		echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)		print_r($_POST);					
if(DEBUG_V)		echo "</pre>";



					// Schritt 2 FORM: Daten auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte werden ausgelesen und entschärft... <i>(" . basename(__FILE__) . ")</i></p>\n";
					
					cleanString($BlogObject->getCategory()->setCatID		(	$_POST['catID']));
					cleanString($BlogObject->setBlogHeadline					(	$_POST['blogHeadline']));
					cleanString($BlogObject->setBlogImageAlignment			(	$_POST['blogImageAlignment']));
					cleanString($BlogObject->setBlogContent					(	$_POST['blogContent']));
					
					
					$catID 					= $BlogObject->getCategory()->getCatID();
					$blogHeadline 			= $BlogObject->getBlogHeadline();
					$blogContent 			= $BlogObject->getBlogImageAlignment();
					$blogImageAlignment 	= $BlogObject->getBlogContent();
if(DEBUG_V) 	echo "<p class='debug value'>Line <b>" . __LINE__ . "</b>: \$catID: $catID <i>(" . basename(__FILE__) . ")</i></p>";
if(DEBUG_V) 	echo "<p class='debug value'>Line <b>" . __LINE__ . "</b>: \$blogHeadline: $blogHeadline <i>(" . basename(__FILE__) . ")</i></p>";
if(DEBUG_V) 	echo "<p class='debug value'>Line <b>" . __LINE__ . "</b>: \$blogImageAlignment: $blogImageAlignment <i>(" . basename(__FILE__) . ")</i></p>";
if(DEBUG_V) 	echo "<p class='debug value'>Line <b>" . __LINE__ . "</b>: \$blogContent: $blogContent <i>(" . basename(__FILE__) . ")</i></p>";

					// Schritt 3 FORM: ggf. Werte validieren
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Feldwerte werden validiert... <i>(" . basename(__FILE__) . ")</i></p>\n";
					$errorHeadline = checkInputString($blogHeadline);
					$errorContent 	= checkInputString($blogContent, minLength:5, maxLength:20000);
					
					#********** FINAL FORM VALIDATION PART I (FIELDS VALIDATION) **********#
					if( $errorHeadline OR $errorContent) {
						// Fehlerfall
if(DEBUG)			echo "<p class='debug err'>Line <b>" . __LINE__ . "</b>: FINAL FORM VALIDATION PART I: Das Formular enthält noch Fehler! <i>(" . basename(__FILE__) . ")</i></p>";
						
					} else {
						
if(DEBUG)			echo "<p class='debug ok'>Line <b>" . __LINE__ . "</b>: FINAL FORM VALIDATION PART I: Das Formular ist formal fehlerfrei. <i>(" . basename(__FILE__) . ")</i></p>";

						#********** CHECK IF IMAGE UPLOAD IS ACTIVE **********#
						
if(DEBUG)			echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Prüfe auf Bildupload... <i>(" . basename(__FILE__) . ")</i></p>\n";
						// Prüfen, ob eine Datei hochgeladen wurde
						// Prüfen, ob eine Datei hochgeladen wurde
						if( $_FILES['blogImage']['tmp_name'] === '' ) {
							// image upload is not active
if(DEBUG)				echo "<p class='debug hint'>Line <b>" . __LINE__ . "</b>: Bildupload ist nicht aktiv. <i>(" . basename(__FILE__) . ")</i></p>";
						
						} else {
							// image upload is active
if(DEBUG)				echo "<p class='debug hint'>Line <b>" . __LINE__ . "</b>: Bild Upload ist aktiv... <i>(" . basename(__FILE__) . ")</i></p>";
							
							$imageUploadResultArray = imageUpload($_FILES['blogImage']['tmp_name']);
					

							#********** VALIDATE IMAGE UPLOAD RESULTS **********#
							if( $imageUploadResultArray['imageError'] ) {
								// Fehlerfall
								$errorImageUpload = $imageUploadResultArray['imageError'];
								
							} else {
								// Erfolgsfall
if(DEBUG)					echo "<p class='debug ok'>Line <b>" . __LINE__ . "</b>: Bild wurde erfolgreich unter <i>'" . $imageUploadResultArray['imagePath'] . "'</i> gespeichert. <i>(" . basename(__FILE__) . ")</i></p>";
								// Pfad zum Bild speichern
								$blogImagePath = $imageUploadResultArray['imagePath'];
								$BlogObject->setBlogImagePath( $blogImagePath );
								
if(DEBUG_V) 			echo "<p class='debug value'>Line <b>" . __LINE__ . "</b>: \$blogImagePath: '{$BlogObject->getBlogImagePath()}' <i>(" . basename(__FILE__) . ")</i></p>";
							
							} // VALIDATE IMAGE UPLOAD END
							
						} // CHECK IF IMAGE UPLOAD END
						
						#*****************************************************#
					
						#********** FINAL IMAGE UPLOAD VALIDATION **********#
						if( $errorImageUpload ) {
							// Fehlerfall
if(DEBUG)				echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FINAL IMAGE UPLOAD VALIDATION: FEHLER: $errorImageUpload <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
						} else {
							// Erfolgsfall
if(DEBUG)				echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: FINAL IMAGE UPLOAD VALIDATION: Die Bildvalidierung ergab keine Fehler. <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
							// Schritt 4 FORM: Daten weiterverarbeiten
if(DEBUG)				echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Daten werden weiterverarbeitet... <i>(" . basename(__FILE__) . ")</i></p>\n";
							
							
							
							#********** SAVE BLOG ENTRY DATA INTO DB **********#
if(DEBUG)				echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Speichere Blogeintrag in DB...  <i>(" . basename(__FILE__) . ")</i></p>\n";
						
								// DB-Verbindung herstellen
								$PDO = DBConnect('bloog_oop');
								$userID = $BlogObject->getUser()->getUserID();
								$rowCount = $BlogObject->saveToDB($PDO, $userID);
								
								if( !$rowCount ) {
								// Fehlerfall
if(DEBUG)					echo "<p class='debug err'>Line <b>" . __LINE__ . "</b>: FEHLER beim Speichern des Blogbeitrags! <i>(" . basename(__FILE__) . ")</i></p>";
								$dbError = 'Es ist ein Fehler aufgetreten! Bitte versuchen Sie es später noch einmal.';
							
							} else {
								// Erfolgsfall
if(DEBUG)					echo "<p class='debug ok'>Line <b>" . __LINE__ . "</b>: Blogbeitrag erfolgreich mit der BlogID '{$BlogObject->getBlogID()}' gespeichert. <i>(" . basename(__FILE__) . ")</i></p>";
								$dbSuccess = 'Der Blogbeitrag wurde erfolgreich gespeichert.';
								
							}
								
						} //FINAL IMAGE UPLOAD VALIDATION END
	
					} // FINAL FORM VALIDATION PART I (FIELDS VALIDATION) END
					
					
					
					

				} // PROCESS FORM 'NEW BLOG ENTRY END
#***************************************************************************************#

				#**********************************************#
				#******** FETCH ALL CATEGORIES FROM DB ********#
				#**********************************************#

if(DEBUG)	echo "<p class='debug'>📑 Line <b>" . __LINE__ . "</b>: Lade Kategorien aus DB... <i>(" . basename(__FILE__) . ")</i></p>";
				
				$PDO = DBConnect('bloog_oop');
				
				$allCategoriesArrayObject = Category::fetchAllFromDB( $PDO );

			
#***************************************************************************************#			
?>

<!doctype html>

<html>

	<head>
		<meta charset="utf-8">
		<title>PHP-Projekt Blog</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/debug.css">
	</head>

	<body class="dashboard">

		<!-- ---------- PAGE HEADER START ---------- -->
	
		<header class="fright">
			<a href="?action=logout">Logout</a><br>
			<a href="index.php"><< zum Frontend</a>
		</header>
		<div class="clearer"></div>

		<br>
		<hr>
		<br>
		
		<!-- ---------- PAGE HEADER END ---------- -->
		
		<h1 class="dashboard">PHP-Projekt Blog - Dashboard</h1>
		
		<p class="name">Aktiver Benutzer: <?= $BlogObject->getUser()->getFullName(); ?></p>
		
		
		<!-- ---------- POPUP MESSAGE START ---------- -->
		<?php if( $dbError OR $dbSuccess ): ?>
		<popupBox>
			<?php if($dbError): ?>
			<h3 class="error"><?= $dbError ?></h3>
			<?php elseif($dbSuccess): ?>
			<h3 class="success"><?= $dbSuccess ?></h3>
			<?php endif ?>
			<a class="button" onclick="document.getElementsByTagName('popupBox')[0].style.display = 'none'">Schließen</a>
		</popupBox>		
		<?php endif ?>
		<!-- ---------- POPUP MESSAGE END ---------- -->
		
		
		
		<!-- ---------- LEFT PAGE COLUMN START ---------- -->
		<main class="forms fleft">			
						
			<h2 class="dashboard">Neuen Blog-Eintrag verfassen</h2>
			<p class="small">
				Um einen Blogeintrag zu verfassen, muss dieser einer Kategorie zugeordnet werden.<br>
				Sollte noch keine Kategorie vorhanden sein, erstellen Sie diese bitte zunächst.
			</p> 
			
			
			<!-- ---------- FORM 'NEW BLOG ENTRY' START ---------- -->
			<form action="" method="POST" enctype="multipart/form-data">
				<input class="dashboard" type="hidden" name="formNewBlogEntry">
				
				<br>
				<label>Kategorie:</label>
				
				<select class="dashboard bold" name="catID">	
				<?php foreach($allCategoriesArrayObject AS $categorySingleItemArrayObject): ?>
					<option value='<?= $categorySingleItemArrayObject->getCatID() ?>' <?php if($catID == $categorySingleItemArrayObject->getCatID()) echo 'selected'?> > <?= $categorySingleItemArrayObject->getCatLabel() ?></option>
				<?php endforeach ?>
				</select> 
				
				<br>
				
				<label>Überschrift:</label>
				<span class="error"><?= $errorHeadline ?></span><br>
				<input class="dashboard" type="text" name="blogHeadline" placeholder="..." value="<?= $blogHeadline ?>"><br>
				
				
				<!-- ---------- IMAGE UPLOAD START ---------- -->
				<label>[Optional] Bild veröffentlichen:</label>
				<span class="error"><?= $errorImageUpload ?></span>
				<imageUpload>					
					
					<!-- -------- INFOTEXT FOR IMAGE UPLOAD START -------- -->
					<p class="small">
						Erlaubt sind Bilder des Typs 
						<?php $allowedMimetypes = implode( ', ', array_keys(IMAGE_ALLOWED_MIME_TYPES) ) ?>
						<?= strtoupper( str_replace( array(', image/jpeg', 'image/'), '', $allowedMimetypes) ) ?>.
						<br>
						Die Bildbreite darf <?= IMAGE_MAX_WIDTH ?> Pixel nicht übersteigen.<br>
						Die Bildhöhe darf <?= IMAGE_MAX_HEIGHT ?> Pixel nicht übersteigen.<br>
						Die Dateigröße darf <?= IMAGE_MAX_SIZE/1024 ?>kB nicht übersteigen.
					</p>
					<!-- -------- INFOTEXT FOR IMAGE UPLOAD END -------- -->
					
					<input type="file" name="blogImage">
					<select class="alignment fright" name="blogImageAlignment">
						<option value="fleft" 	<?php if($blogImageAlignment == 'fleft') echo 'selected'?>>align left</option>
						<option value="fright" 	<?php if($blogImageAlignment == 'fright') echo 'selected'?>>align right</option>
					</select>
				</imageUpload>
				<br>	
				<!-- ---------- IMAGE UPLOAD END ---------- -->
				
				
				<label>Inhalt des Blogeintrags:</label>
				<span class="error"><?= $errorContent ?></span><br>
				<textarea class="dashboard" name="blogContent" placeholder="..."><?= $blogContent ?></textarea><br>
				
				<div class="clearer"></div>
				
				<input class="dashboard" type="submit" value="Veröffentlichen">
			</form>
			<!-- ---------- FORM 'NEW BLOG ENTRY' END ---------- -->
			
		</main>
		<!-- ---------- LEFT PAGE COLUMN END ---------- -->
		
		
		
		<!-- ---------- RIGHT PAGE COLUMN START ---------- -->
		<aside class="forms fright">
		
			<h2 class="dashboard">Neue Kategorie anlegen</h2>
			
			
			<!-- ---------- FORM 'NEW CATEGORY' START ---------- -->			
			<form class="dashboard" action="" method="POST">
			
				<input class="dashboard" type="hidden" name="formNewCategory">
				
				<label>Name der neuen Kategorie:</label>
				<span class="error"><?= $errorCatLabel ?></span><br>
				<input class="dashboard" type="text" name="catLabel" placeholder="..." value="<?= $BlogObject->getCategory()->getCatLabel() ?>"><br>

				<input class="dashboard" type="submit" value="Neue Kategorie anlegen">
			</form>
			<!-- ---------- FORM 'NEW CATEGORY' END ---------- -->
			
		
		</aside>

		<div class="clearer"></div>
		<!-- ---------- RIGHT PAGE COLUMN END ---------- -->
		
		
	</body>
</html>






