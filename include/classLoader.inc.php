<?php
#*******************************************************************************************#
				
				
				function classLoader($name) {
if(DEBUG_F)		echo "<p class='debugClassLoader'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$name') <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					
					#********** CHECK IF $name IS TRAIT **********#
					/*
						Da wir lediglich den Namen des Traits, aber keine Dateiendung oder dergleichen
						übergeben bekommen, haben wir anhand von $name keine Möglichkeit zu prüfen,
						ob es sich bei der angeforderten Datei um einen Trait handelt.
						Um dieses Problem zu lösen, benötigen wir eine Liste mit existierenden Trait-Namen,
						die wir zum Abgleich verwenden können: Steht der angeforderte Name in der Trait-Liste, 
						handelt es sich um einen Trait.
					*/
					if( in_array($name, TRAIT_NAMES) === true ) {
						$filePath = TRAIT_PATH . $name . TRAIT_FILE_EXTENSION;
						$fileType = 'TRAIT';
					
					
					#********** CHECK IF $name IS INTERFACE **********#
					} elseif( str_ends_with($name, 'Interface') ) {
						$filePath = INTERFACE_PATH . $name . INTERFACE_FILE_EXTENSION;
						$fileType = 'INTERFACE';
						
					
					#********** CHECK IF $name IS CLASS **********#
					} else {
						$filePath = CLASS_PATH . $name . CLASS_FILE_EXTENSION;
						$fileType = 'CLASS';
					}
										
// if(DEBUG_V)		echo "<p class='debugClassLoader value'><b>Line " . __LINE__ . "</b>: \$filePath: '$filePath' <i>(" . basename(__FILE__) . ")</i></p>\n";


					#********** CHECK IF FILE EXISTS **********#
					if( file_exists($filePath) === false ) {
						// Fehlerfall
if(DEBUG_F)			echo "<p class='debugClassLoader err'><b>Line " . __LINE__ . "</b>: FEHLER: $fileType '$name' unter <i>'$filePath'</i> nicht gefunden! <i>(" . basename(__FILE__) . ")</i></p>\n";				
						echo '<h2 class="error">Auf der Seite ist ein Fehler aufgetreten! Bitte versuchen Sie es später noch einmal.</h2>';
						exit;
						
					} else {
						// Erfolgsfall
if(DEBUG_F)			echo "<p class='debugClassLoader ok'><b>Line " . __LINE__ . "</b>: $fileType '$name' unter <i>'$filePath'</i> gefunden. <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
						
						#********** FINALLY INCLUDE FILE **********#
						require_once($filePath);
					}
				}
				
				
#*******************************************************************************************#
?>