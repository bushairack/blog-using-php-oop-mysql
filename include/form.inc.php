<?php
#********************************************************************************************#
				
				
				/**
				*
				*	Ersetzt potentiell gefährliche Zeichen (< > " ' &) eines übergebenen Strings
				*	durch HTML-Entities und entfernt alle Whitespaces vor und nach dem String.
				*	Ersetzt einen übergebenen Leerstring durch NULL
				*
				*	@param	String	$value		Der übergebene String
				*
				*	@return	NULL|String				NULL bei übergebenem Leerstring | Der entschärfte und bereinigte String
				*
				*/
				function cleanString($value) {
if(DEBUG_F)		echo "<p class='debugCleanString'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$value') <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					/*
						trim() entfernt VOR und NACH einem String (aber nicht mitten drin) 
						sämtliche sog. Whitespaces (Leerzeichen, Tabs, Zeilenumbrüche)
					*/
					$value = trim($value);
					
					/*
						SCHUTZ GEGEN EINSCHLEUSUNG UNERWÜNSCHTEN CODES:
						Damit so etwas nicht passiert: <script>alert("HACK!")</script>
						muss der empfangene String ZWINGEND entschärft werden!
						htmlspecialchars() wandelt potentiell gefährliche Steuerzeichen wie
						< > " & in HTML-Code um (&lt; &gt; &quot; &amp;).
						
						Der Parameter ENT_QUOTES wandelt zusätzlich einfache ' in &apos; um.
						Der Parameter ENT_HTML5 sorgt dafür, dass der generierte HTML-Code HTML5-konform ist.
						
						Der 1. optionale Parameter regelt die zugrundeliegende Zeichencodierung 
						(NULL=Zeichencodierung wird vom Webserver übernommen)
						
						Der 2. optionale Parameter bestimmt die Zeichenkodierung
						
						Der 3. optionale Parameter regelt, ob bereits vorhandene HTML-Entities erneut entschärft werden
						(false=keine doppelte Entschärfung)
					*/
					$value = htmlspecialchars($value, ENT_QUOTES | ENT_HTML5, 'UTF-8', false);
					
					/*
						Damit cleanString() nicht übergebene NULL-Werte in Leerstings verändert, wird 
						ein am Ende eventuell vorhandener Leerstring in $value mit NULL überschrieben 
					*/
					if( $value === '' ) {
						$value = NULL;
					}
					
					return $value;
				}
				

#********************************************************************************************#

				
				/**
				*
				*	Prüft einen übergebenen String auf Mindestlänge und Maximallänge sowie optional 
				* 	zusätzlich auf Leerstring.
				*	Generiert Fehlermeldung bei Leerstring oder ungültiger Länge
				*
				*	@param	String		$value									Der übergebene String
				*	@param	Bool			$mandatory=true						Angabe zu Pflichteingabe	
				*	@param	Integer		$minLength=INPUT_MIN_LENGTH		Die zu prüfende Mindestlänge								
				*	@param	Integer		$maxLength=INPUT_MAX_LENGTH		Die zu prüfende Maximallänge								
				*
				*	@return	String|NULL												Fehlermeldung | ansonsten NULL
				*
				*/
				function checkInputString($value, $mandatory=true, $minLength=INPUT_MIN_LENGTH, $maxLength=INPUT_MAX_LENGTH) {
if(DEBUG_F)		echo "<p class='debugCheckInputString'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$value' | [$minLength | $maxLength] | mandatory: $mandatory) <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					/*
						In der Programmierung müssen Werte aus Variablen immer wieder geprüft werden.
						Beispielsweise im Rahmen einer Feldvalidierung für Formulare.
						Hierzu wird der Wert einer Variablen auf seinen bool'schen Zustand (also true oder false)
						interpretiert:
						Als false werden interpretiert: Leerstring, Integer 0, Float 0.0, NULL, ein leeres Array oder 
						der tatsächliche bool'sche Wert false.
						Ausnahme: Ein String mit einer (!) Null ('0') wird als false interpretiert, '00' sind hingegen true
					*/
					// Optional (wenn $mandatory===true): Prüfen auf Leerstring oder NULL
					#********** MANDATORY CHECK **********#
					if( $mandatory === true AND ($value === '' OR $value === NULL) ) {  // $value === NULL muss ergänzt werden, wenn cleanString() auf NULL-Rückgabe umgestellt wird.
						// Fehlerfall
						return 'Dies ist ein Pflichtfeld!';
						
					
					#********** MINIMUM LENGTH CHECK **********#
					} elseif( mb_strlen($value) < $minLength ) {
						// Fehlerfall
						return "Muss mindestens $minLength Zeichen lang sein!";
						
						
					#********** MAXIMUM LENGTH CHECK **********#
					} elseif( mb_strlen($value) > $maxLength ) {
						// Fehlerfall
						return "Darf maximal $maxLength Zeichen lang sein!";
						
					
					#********** STRING IS VALID **********#
					} else {
						// Erfolgsfall
						return NULL;
					}				
				}


#********************************************************************************************#

				
				/**
				*
				*	Prüft einen übergebenen String auf Leerstring und Maximallänge
				*	Prüft den übergebene String zusätzlich auf eine valide Email-Adresse
				*	Generiert Fehlermeldung bei Leerstring oder ungültiger Maximallänge 
				*	oder ungültiger Email-Adresse
				*
				*	@param	String	$value									Der übergebene String
				*	@param	Bool		$mandatory=true						Pflichteingabe	
				*	@param	Integer	$maxLength=INPUT_MAX_LENGTH		Die zu prüfende Maximallänge
				*
				*	@return	String|NULL											Fehlermeldung | ansonsten NULL
				*
				*/
				function validateEmail($value, $mandatory=true, $minLength=INPUT_MIN_LENGTH, $maxLength=INPUT_MAX_LENGTH) {
if(DEBUG_F)		echo "<p class='debugValidateEmail'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$value' | [$minLength | $maxLength] | mandatory: $mandatory) <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					
					#********** VALIDATE MANDATORY AND MAXIMUM LENGTH **********#
					// Prüfen auf Leerstring und Maximallänge
					if( $error = checkInputString($value, $mandatory, $minLength, $maxLength) ) {
						// Fehlerfall
						return $error;
						
					
					#********** VALIDATE EMAIL ADDRESS **********#
					} elseif( filter_var($value, FILTER_VALIDATE_EMAIL) === false ) {
						// Fehlerfall
						return 'Dies ist keine gültige Email-Adresse!';
					
					
					#********** STRING IS VALID EMAIL ADDRESS **********#
					} else {
						// Erfolgsfall
						return NULL;
					}
				}


#********************************************************************************************#


				function validatePrice($value, $mandatory=true) {
if(DEBUG_F)		echo "<p class='debugValidateFloat'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$value' | mandatory: $mandatory) <i>(" . basename(__FILE__) . ")</i></p>\n";	
									
					/*
						ANALYSE:
						Dezimaltrennzeichen: Komma oder Punkt
						Das Dezimaltrennzeichen plus Nachkommastellen sollen für glatte Preise optional sein
						Bei Eingabe von Nachkommastellen muss zwingend das Dezimaltrennzeichen gesetzt sein
						
						VOR dem Dezimaltrennzeichen: Mindestens 1x und maximal 4x Ziffer 0-9
						Dezimaltrennzeichen: optional 1 Komma oder 1 Punkt
						NACH dem Dezimaltrennzeichen: optional maximal 2x Ziffern 0-9
						
						Der GESAMTE String muss diesmal dem Muster entsprechen. Nicht nur ein Teil des Strings!
						Das ^-Zeichen zeigt an, dass der zu untersuchende String an diesem Punkt des Musters beginnen muss.
						Das $-Zeichen zeigt an, dass der zu untersuchende String an diesem Punkt des Musters enden muss.
						Die Kombination aus Beginn und Ende ergibt, dass der gesamte String dem Muster entsprechen muss.
					
						Das ? bedeutet, dass das vor ihm notierte Element nicht oder genau 1 Mal vorkommen muss
						Das x am Ende des Patterns bedeutet, dass alle Whitespaces zwischen den Elementen ignoriert werden
					*/
					$pattern 	= '/^   [0-9]{1,10}   ([\.,]   [0-9]{1,2})?   $/x';
					
					
					#********** CHECK FOR EMPTY VALUE AND MAXIMUM LENGTH **********#
					if( $error = checkInputString($value, $mandatory) ) {
						// Fehlerfall
						return $error;
					
					
					#********** VALIDATE PRICE **********#
					/*
						Die Funktion preg_match() prüft, ob ein Haystack ein definiertes RegEx-Muster enthält.
						Bei einem Treffer gibt die Funktion den Integer 1 zurück.
						Bei keinem Treffer gibt die Funktion den Integer 0 zurück.
						Bei einem ungültigen Muster (fehlerhafte Syntax) gibt die Funktion den Boolean false 
						zurück.
					*/
					} elseif( preg_match($pattern, $value) === 0 ) {
						// Kein Treffer
						return 'Dies ist kein gültiger Preis!';
						
					} elseif( preg_match($pattern, $value) === false ) {
						// ungültiges Muster
if(DEBUG_F)			echo "<p class='debugValidateFloat err'><b>Line " . __LINE__ . "</b>: Fehlerhaftes RegEx-Pattern! <i>(" . basename(__FILE__) . ")</i></p>\n";				
						return 'Es ist ein interner Fehler aufgetreten!';
							
					} elseif( preg_match($pattern, $value) === 1 ) {
						// Treffer
						return NULL;
					}	
				}


#********************************************************************************************#


				function validateInt($value, $minValue=0, $maxValue=2147483647, $mandatory=true) {
if(DEBUG_F)		echo "<p class='debugValidateInt'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$value' | [$minValue | $maxValue]) <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					/*
						Datentypprüfung als weiteres Sicherheitsmerkmal:
						Als Präventivmaßnahme geprüft, ob beispielsweise eine ID tatsächlich einer echten ID (also einem
						reinen Integer) entspricht.
						
						Die Funktion filter_var() mit der Konstante FILTER_VALIDATE_INT prüft den 
						Inhalt eines Strings auf einen Integer-Wert, ohne ihr dabei umzuwandeln und 
						damit ggf. negative Umwandlungseffekte in Kauf zu nehmen.
						Nach erfolgreicher Inhaltsprüfung kann der Wert dann sicher mittels der Funktion 
						intval() in einen echten Integer umgewandelt und somit intern mit einem Integer 
						weitergearbeitet werden.
						
						Das Ganze funktioniert auch mit dem Datentyp Float. Andere Datentypen machen hier 
						keinen Sinn.
					*/
					
					#********** CHECK FOR EMPTY VALUE AND MAXIMUM LENGTH **********#
					if( $error = checkInputString($value, $mandatory) ) {
						// Fehlerfall
						return $error;
						
						
					#********** VALIDATE DATA TYPE CONFORMITY **********#
					} elseif( filter_var($value, FILTER_VALIDATE_INT) === false ) {
						// Fehlerfall
						return 'Dies ist kein gültiger Wert!';
					
					
					#********** VALIDATE RANGE OF VALUE **********#
					} elseif( $value < $minValue OR $value > $maxValue ) {
						// Fehlerfall
						return 'Dies ist kein gültiger Wert!';
						
					} else {
						// Erfolgsfall
						return NULL;
					}
				}


#********************************************************************************************#

				
				/**
				*
				*	Validiert ein auf den Server geladenes Bild, generiert einen unique Dateinamen
				*	sowie eine sichere Dateiendung und verschiebt das Bild in ein anzugebendes Zielverzeichnis.
				*	Validiert werden der aus dem Dateiheader ausgelesene MIME-Type, die aus dem Dateiheader
				*	ausgelesene Bildgröße in Pixeln sowie die ermittelte Dateigröße. 
				*	Der Dateiheader wird außerdem auf Plausibilität geprüft.
				*
				*	@param	String	$fileTemp														Der temporäre Pfad zum hochgeladenen Bild im Quarantäneverzeichnis
				*	@param	Integer	$imageMaxWidth=IMAGE_MAX_WIDTH							Die maximal erlaubte Bildbreite in Pixeln
				*	@param	Integer	$imageMaxHeight=IMAGE_MAX_HEIGHT							Die maximal erlaubte Bildhöhe in Pixeln
				*	@param	Integer	$imageMaxSize=IMAGE_MAX_SIZE								Die maximal erlaubte Dateigröße in Bytes
				*	@param	String	$imageUploadPath=IMAGE_UPLOAD_PATH						Das Zielverzeichnis
				*	@param	Array		$imageAllowedMimeTypes=IMAGE_ALLOWED_MIME_TYPES		Whitelist der zulässigen MIME-Types mit den zugehörigen Dateiendungen
				*	@param	Integer	$imageMinSize=IMAGE_MIN_SIZE								Die minimal erlaubte Dateigröße in Bytes
				*
				*	@return	Array		{'imagePath'	=>	String|NULL, 							Bei Erfolg der Speicherpfad zur Datei im Zielverzeichnis | bei Fehler NULL
				*							 'imageError'	=>	String|NULL}							Bei Erfolg NULL | Bei Fehler Fehlermeldung
				*
				*/
				function imageUpload( $fileTemp,
											 $imageMaxWidth				= IMAGE_MAX_WIDTH,
											 $imageMaxHeight				= IMAGE_MAX_HEIGHT,
											 $imageMaxSize					= IMAGE_MAX_SIZE,
											 $imageUploadPath				= IMAGE_UPLOAD_PATH,
											 $imageAllowedMimeTypes		= IMAGE_ALLOWED_MIME_TYPES,
											 $imageMinSize					= IMAGE_MIN_SIZE
											) {
if(DEBUG_F)		echo "<p class='debugImageUpload'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$fileTemp') <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					
					#***************************************************************************#
					#********** GATHER INFORMATION FOR IMAGE FILE VIA THE FILE HEADER **********#
					#***************************************************************************#
					
					/*
						Die Funktion getimagesize() liest den Dateiheader einern Bilddatei aus und 
						liefert bei gültigem MIME Type ('image/...') ein gemischtes Array zurück:
						
						[0] 				Bildbreite in PX 
						[1] 				Bildhöhe in PX 
						[3] 				Einen für das HTML <img>-Tag vorbereiteten String (width="480" height="532") 
						['bits']			Anzahl der Bits pro Kanal 
						['channels']	Anzahl der Farbkanäle (somit auch das Farbmodell: RGB=3, CMYK=4) 
						['mime'] 		MIME Type
						
						Bei ungültigem MIME Type (also nicht 'image/...') liefert getimagesize() false zurück
					*/
					$imageDataArray = getimagesize($fileTemp);
/*					
if(DEBUG_V)		echo "<pre class='debugImageUpload value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)		print_r($imageDataArray);					
if(DEBUG_V)		echo "</pre>";
*/					
					
					#********** CHECK FOR VALID MIME TYPE **********#
					if( $imageDataArray === false ) {
						// Fehlerfall (MIME TYPE is not valid)
						/*
							Bildwerte auf NULL setzen, damit die Variablen für die nachfolgenden
							Validierungen exitieren und zu korrekten Fehlermeldungen führen
						*/
						$imageWidth = $imageHeight = $imageMimeType = $fileSize = NULL;
					

					#********** FETCH FILE INFOS **********#
					} elseif( is_array($imageDataArray) === true ) {
						// Erfolgsfall (MIME TYPE is valid)
						
						$imageWidth 	= $imageDataArray[0];			// image width in px via getimagesize()
						$imageHeight 	= $imageDataArray[1];			// image height in px via getimagesize()
						$imageMimeType	= $imageDataArray['mime'];		// image mime type via getimagesize()
						$fileSize		= filesize($fileTemp);			// image size in bytes via filesize()
					}
if(DEBUG_V)		echo "<p class='debugImageUpload value'><b>Line " . __LINE__ . "</b>: \$imageWidth: $imageWidth px <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)		echo "<p class='debugImageUpload value'><b>Line " . __LINE__ . "</b>: \$imageHeight: $imageHeight px <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)		echo "<p class='debugImageUpload value'><b>Line " . __LINE__ . "</b>: \$imageMimeType: $imageMimeType <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)		echo "<p class='debugImageUpload value'><b>Line " . __LINE__ . "</b>: \$fileSize: " . round($fileSize/1024, 2) . " kB <i>(" . basename(__FILE__) . ")</i></p>\n";
					
					#*************************************************#
					
					
					#**************************************#
					#********** IMAGE VALIDATION **********#
					#**************************************#
					
					// Whitelist mit erlaubten MIME TYPES und Dateiendungen
					// $imageAllowedMimeTypes = array('image/jpg' => '.jpg', 'image/jpeg' => '.jpg', 'image/png' => '.png', 'image/gif' => '.gif');
					
					/*
						Da Schadcode häufig nur wenige Zeilen lang ist, ist eine zu kleine
						Dateigröße per se verdächtig. Brauchbare Bilddateien beginnen bei
						etwa 1kB Dateigröße (ca. 80-100Bytes für Icons).
						Außerdem wird gleich geprüft, ob ein Hacker womöglich den MIME Type
						im Dateiheader manipuliert hat. Bilder verfügen immer über eine Größenangabe
						in Pixeln, die vom Hacker manchmal vergessen wird, ebenfalls in den manipulierten
						Header einzufügen. Wenn die Bildgrößenangaben keinen Wert besitzen, muss von einem
						manipulierten Dateiheader ausgegangen werden.
						
						Sollte getimagesize() aufgrund eines falschen MIME Types 'false' zurückgeliefert haben,
						wurden im vorherigen Schritt alle Variablenwerte auf NULL gesetzt und führen hier 
						automatisch zum Fehlerfall.
					*/
					#********** CHECK IF FILE HEADER IS PLAUSIBLE **********#
					if( $fileSize < $imageMinSize OR $imageWidth === NULL OR $imageHeight === NULL OR $imageMimeType === NULL ) {
						// Fehlerfall 1: Potentiell verdächtiger Dateiheader
						$errorMessage = 'Potentielles Schadskript entdeckt!';
					
						
					#********** CHECK FOR VALID MIME TYPE **********#
					/*
						Der optionale 3. Parameter der in_array()-Funktion erzwingt einen strikten Wertevergleich, 
						damit '0' und '' nicht als gleich interpretiert werden.
						Er sollte aus Sicherheitsgründen immer gesetzt werden.
						in_array() liefert 'true' zurück, wenn die Needle im Array gefunden wurde, ansonsten 'false'.
					*/
					} elseif( in_array($imageMimeType, array_keys($imageAllowedMimeTypes), true) === false ) {
						// Fehlerfall 2: Unerlaubter MIME TYPE
						$errorMessage = 'Dies ist kein erlaubter Bildtyp!';
					
					
					#********** VALIDATE IMAGE WIDTH **********#
					} elseif( $imageWidth > $imageMaxWidth ) {
						// Fehlerfall 3: Bildbreite zu groß
						$errorMessage = "Die Bildbreite darf maximal $imageMaxWidth Pixel betragen!";
					
					
					#********** VALIDATE IMAGE HEIGHT **********#
					} elseif( $imageHeight > $imageMaxHeight ) {
						// Fehlerfall 4: Bildhöhe zu groß
						$errorMessage = "Die Bildhöhe darf maximal $imageMaxHeight Pixel betragen!";
						
						
					#********** VALIDATE FILE SIZE **********#	
					} elseif( $fileSize > $imageMaxSize ) {
						// Fehlerfall 5: Dateigröße zu groß
						$errorMessage = 'Die Dateigröße darf maximal ' . $imageMaxSize/1024 . 'kB betragen!';
						
					
					#********** ALL CHECKS ARE PASSED **********#
					} else {
						// Erfolgsfall
						$errorMessage = NULL;
					}
					
					#*************************************************#
					
					
					#********** FINAL IMAGE VALIDATION **********#
					if( $errorMessage !== NULL ) {
						// Fehlerfall
if(DEBUG_F)			echo "<p class='debugImageUpload err'><b>Line " . __LINE__ . "</b>: $errorMessage <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
						/*
							Da die Verzweigung im Fehlerfall an dieser Stelle verlassen wird, die Variable
							$fileTarget aber fester Bestandteil des Return-Wertes ist, muss sie an dieser 
							Stelle initialisiert werden, da sie ansonsten nicht existiert.
						*/
						// Initialize $fileTarget
						$fileTarget = NULL;
						
					} else {
						// Erfolgsfall
if(DEBUG_F)			echo "<p class='debugImageUpload ok'><b>Line " . __LINE__ . "</b>: Die Bildvalidierung ergab keinen Fehler. <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
						
						#**********************************************************#
						#********** PREPARE IMAGE FOR PERSISTANT STORING **********#
						#**********************************************************#
						
						/*
							Da der Dateiname selbst Schadcode in Form von ungültigen oder versteckten Zeichen,
							doppelte Dateiendungen (dateiname.exe.jpg) etc. beinhalten kann, darüberhinaus ohnehin 
							sämtliche, nicht in einer URL erlaubten Sonderzeichen und Umlaute entfernt werden müssten 
							sollte der Dateiname aus Sicherheitsgründen komplett neu generiert werden.
							
							Hierbei muss außerdem bedacht werden, dass die jeweils generierten Dateinamen unique
							sein müssen, damit die Dateien sich bei gleichem Dateinamen nicht gegenseitig überschreiben.
						*/
						
						#********** GENERATE UNIQUE FILE NAME **********#
						/*
							- 	mt_rand() stellt die verbesserte Version der Funktion rand() dar und generiert 
								Zufallszahlen mit einer gleichmäßigeren Verteilung über das Wertesprektrum. Ohne zusätzliche
								Parameter werden Zahlenwerte zwischen 0 und dem höchstmöglichem von mt_rand() verarbeitbaren 
								Zahlenwert erzeugt.							
							- 	str_shuffle mischt die Zeichen eines übergebenen Strings zufällig durcheinander.
							- 	microtime() liefert einen Timestamp mit Millionstel Sekunden zurück (z.B. '0.57914300 163433596'),
								aus dem für eine URL-konforme Darstellung der Dezimaltrenner und das Leerzeichen entfernt werden.
						*/
						$fileName = mt_rand() . '_' . str_shuffle('abcdefghijklmnopqrstuvwxyz_-0123456789') . '_' . str_replace( array('.', ' '), '', microtime());
						
						
						#********** GENERATE FILE EXTENSION **********#
						/*
							Aus Sicherheitsgründen wird nicht die ursprüngliche Dateinamenerweiterung aus dem
							Dateinamen verwendet, sondern eine vorgenerierte Dateiendung aus dem Array der 
							erlaubten MIME Types.
							Die Dateiendung wird anhand des ausgelesenen MIME Types [key] ausgewählt.
						*/
						$fileExtension = $imageAllowedMimeTypes[$imageMimeType];
						
						
						#********** GENERATE FILE TARGET **********#
						/*
							Endgültigen Speicherpfad auf dem Server generieren:
							destinationPath/fileName + fileExtension
						*/
						$fileTarget = $imageUploadPath . $fileName . $fileExtension;						
					
					
if(DEBUG_V)			echo "<p class='debugImageUpload value'><b>Line " . __LINE__ . "</b>: \$fileTarget: <i>'$fileTarget'</i> <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)			echo "<p class='debugImageUpload value'><b>Line " . __LINE__ . "</b>: Länge des Pfades: " . strlen($fileTarget) . " <i>(" . basename(__FILE__) . ")</i></p>\n";
					
						#*************************************************#
						
						
						#*****************************************************#
						#********** MOVE IMAGE TO FINAL DESTINATION **********#
						#*****************************************************#
						
						/*
							move_uploaded_file() verschiebt eine hochgeladene Datei an einen 
							neuen Speicherort und benennt die Datei um
						*/
						if( move_uploaded_file($fileTemp, $fileTarget) === false ) {
							// Fehlerfall
if(DEBUG_F)				echo "<p class='debugImageUpload err'><b>Line " . __LINE__ . "</b>: FEHLER beim Verschieben der Datei von '$fileTemp' nach '$fileTarget'! <i>(" . basename(__FILE__) . ")</i></p>\n";				
							$errorMessage = 'Beim Speichern des Bildes ist ein Fehler aufgetreten! Bitte versuchen Sie es später noch einmal.';
							
							// Lösche $fileTarget
							$fileTarget = NULL;
							
						} else {
							// Erfolgsfall
if(DEBUG_F)				echo "<p class='debugImageUpload ok'><b>Line " . __LINE__ . "</b>: Datei erfolgreich von <i>'$fileTemp'</i> nach <i>'$fileTarget'</i> verschoben. <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
						} // MOVE IMAGE TO FINAL DESTINATION END					
						#*************************************************#
					
					} // FINAL IMAGE VALIDATION END
					
					#*************************************************#
					
					#********** RETURN ARRAY CONTAINING EITHER IMAGE PATH OR ERROR MESSAGE **********#
					return array( 'imagePath' => $fileTarget, 'imageError' => $errorMessage );
					
					#*************************************************#
					
				}


#********************************************************************************************#
?>


















