<?php
#********************************************************************************************#

				
				#****************************************#
				#********** SECURE PAGE ACCESS **********#
				#****************************************#				
				
				/**
				*
				*	Sichert eine Seite gegen unbefugten Zutritt anhand einer Session.
				*	Der Name der Session muss übergeben werden.
				*	Der Zugang wird über den beim Login zu vergebenden Session-Index 'ID' validiert.
				*	Zusätzlich wird die IP-Adresse über den beim Login zu vergebenden Session-Index 'IPAddress' validiert.
				*	Bei ungültigem Login wird der User über die Funktion logout() auf die Startseite zurückgeworfen.
				*
				*	@params	String	$sessionName		Der zu verwendende Session-Name
				*
				*	@return	Int								Die ID des eingeloggten Users aus der Session
				*
				*/
				function securePageAccess($sessionName) {
if(DEBUG_F)		echo "<p class='debugAuth'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$sessionName') <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					/*
						Für die Fortsetzung der Session muss hier der gleiche Name ausgewählt werden,
						wie beim Login-Vorgang, damit die Seite weiß, welches Cookie sie vom Client auslesen soll
					*/
					session_name($sessionName);
					
					
					#********** START/CONTINUE SESSION **********#
					/*
						Der Befehl session_start() liest zunächst ein Cookie aus dem Browser des Clients aus,
						das den Nanem des im oberen Schritts gesetzten Sessionnamens entspricht. Existiert
						dieses Cookie, wird aus ihm der Name der zugehörigen Sessiondatei ausgelesen und geprüft,
						ob diese auf dem Server existiert. Ist beides der Fall, wird die bestehende Session fortgesetzt.
						
						Existieren Cookie oder Sessiondatei nicht, wird an dieser Stelle eine neue Session
						gestartet: Der Browser erhält ein frisches Cookie mit dem oben gesetzten Namen, und auf dem Server
						wird eine neue, leere Sessiondatei erstellt, deren Dateinamen in das Cookie geschrieben wird.
					*/
					if( session_start() === false ) {
						// Fehlerfall
if(DEBUG_F)			echo "<p class='debugAuth err'><b>Line " . __LINE__ . "</b>: FEHLER beim Starten der Session! <i>(" . basename(__FILE__) . ")</i></p>\n";				
										
					} else {
						// Erfolgsfall
if(DEBUG_F)			echo "<p class='debugAuth ok'><b>Line " . __LINE__ . "</b>: Session <i>'$sessionName'</i> erfolgreich gestartet. <i>(" . basename(__FILE__) . ")</i></p>\n";				
/*				
if(DEBUG_F)			echo "<pre class='debugAuth value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_F)			print_r($_SESSION);					
if(DEBUG_F)			echo "</pre>";
*/
					
						#********** CHECK FOR VALID LOGIN **********#
						/*
							Ohne erfolgten Login ist das SESSION-Array an dieser Stelle leer.
							Bei erfolgtem Login beinhaltet das SESSION-Array an dieser Stelle 
							den beim Login-Vorgang vergebenen Index 'ID', der hier auf Existenz geprüft wird.
						*/
						
						#********** A) NO VALID LOGIN **********#
						/*
							SICHERHEIT: Um Session Hijacking und ähnliche Identitätsdiebstähle zu verhindern,
							wird die IP-Adresse des sich einloggenden Users beim Loginvorgang in die Session gespeichert.
							Hier wird die aufrufende IP-Adresse erneut ermittelt und mit der in der Session gespeicherten 
							IP-Adresse abgeglichen.
							Eine IP-Adresse zu fälschen ist nahezu unmöglich. Wenn sich also ein Cookie-Dieb von einer
							anderen IP-Adresse als der beim Loginvorgang aktuellen aus einloggen will, wird ihm an dieser Stelle
							der Zugang verweigert und der Login muss erneut durchgeführt werden.
						*/
						if( !isset($_SESSION['ID']) OR $_SESSION['IPAddress'] !== $_SERVER['REMOTE_ADDR'] ) {
							// Fehlerfall | User ist nicht eingeloggt
if(DEBUG_F)				echo "<p class='debugAuth err'><b>Line " . __LINE__ . "</b>: User-Login ist NICHT validiert! <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
						
							#********** DENY PAGE ACCESS **********#
						//logout();						
					

						#********** B) VALID LOGIN **********#
						} else {
							// Erfolgsfall | User ist eingeloggt
if(DEBUG_F)				echo "<p class='debugAuth ok'><b>Line " . __LINE__ . "</b>: User-Login ist validiert. <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
							/*
								SICHERHEIT: Um Cookiediebstahl oder Session Hijacking vorzubeugen, wird nach erfolgreicher
								Authentifizierung eine neue Session-ID vergeben. Ein Hacker, der zuvor ein Cookie mit einer 
								gültigen Session-ID erbeutet hat, kann dieses nun nicht mehr benutzen.
								Die Session-ID muss bei jedem erfolgreichem Login und bei jedem Logout erneuert werden, um
								einen effektiven Schutz zu bieten.
								
								Um die alte Session mit der alten (abgelaufenen) ID gleich zu löschen und eine neue Session
								mit einer neuen ID zu generieren, muss session_regenerate_id() den optionalen Parameter 
								delete_old_session=true erhalten.
							*/
							session_regenerate_id(true);
							
							// ID des eingeloggten Users als Integer zurückgeben
							return intval($_SESSION['ID']);
							
						} // CHECK FOR VALID LOGIN END
					
					} // SECURE PAGE ACCESS END					
				}


#********************************************************************************************#


				/**
				*
				*	Sichert eine Seite gegen unbefugten Zutritt anhand einer Session.
				*	Der Name der Session muss übergeben werden.
				*	Der Zugang wird über den beim Login zu vergebenden Session-Index 'ID' validiert.
				*	Zusätzlich wird die IP-Adresse über den beim Login zu vergebenden Session-Index 'IPAddress' validiert.
				*	Bei ungültigem Login wird der User über die Funktion logout() auf die Startseite zurückgeworfen.
				*
				*	@params	String	$sessionName		Der zu verwendende Session-Name
				*
				*	@return	Int								Die ID des eingeloggten Users aus der Session
				*
				*/
				function checkLogin($sessionName) {
if(DEBUG_F)		echo "<p class='debugAuth'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('$sessionName') <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					/*
						Für die Fortsetzung der Session muss hier der gleiche Name ausgewählt werden,
						wie beim Login-Vorgang, damit die Seite weiß, welches Cookie sie vom Client auslesen soll
					*/
					session_name($sessionName);
					
					
					#********** START/CONTINUE SESSION **********#
					/*
						Der Befehl session_start() liest zunächst ein Cookie aus dem Browser des Clients aus,
						das den Nanem des im oberen Schritts gesetzten Sessionnamens entspricht. Existiert
						dieses Cookie, wird aus ihm der Name der zugehörigen Sessiondatei ausgelesen und geprüft,
						ob diese auf dem Server existiert. Ist beides der Fall, wird die bestehende Session fortgesetzt.
						
						Existieren Cookie oder Sessiondatei nicht, wird an dieser Stelle eine neue Session
						gestartet: Der Browser erhält ein frisches Cookie mit dem oben gesetzten Namen, und auf dem Server
						wird eine neue, leere Sessiondatei erstellt, deren Dateinamen in das Cookie geschrieben wird.
					*/
					if( session_start() === false ) {
						// Fehlerfall
if(DEBUG_F)			echo "<p class='debugAuth err'><b>Line " . __LINE__ . "</b>: FEHLER beim Starten der Session! <i>(" . basename(__FILE__) . ")</i></p>\n";				
										
					} else {
						// Erfolgsfall
if(DEBUG_F)			echo "<p class='debugAuth ok'><b>Line " . __LINE__ . "</b>: Session <i>'$sessionName'</i> erfolgreich gestartet. <i>(" . basename(__FILE__) . ")</i></p>\n";				
/*				
if(DEBUG_F)			echo "<pre class='debugAuth value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_F)			print_r($_SESSION);					
if(DEBUG_F)			echo "</pre>";
*/
					
						#********** CHECK FOR VALID LOGIN **********#
						/*
							Ohne erfolgten Login ist das SESSION-Array an dieser Stelle leer.
							Bei erfolgtem Login beinhaltet das SESSION-Array an dieser Stelle 
							den beim Login-Vorgang vergebenen Index 'ID', der hier auf Existenz geprüft wird.
						*/
						
						#********** A) NO VALID LOGIN **********#
						/*
							SICHERHEIT: Um Session Hijacking und ähnliche Identitätsdiebstähle zu verhindern,
							wird die IP-Adresse des sich einloggenden Users beim Loginvorgang in die Session gespeichert.
							Hier wird die aufrufende IP-Adresse erneut ermittelt und mit der in der Session gespeicherten 
							IP-Adresse abgeglichen.
							Eine IP-Adresse zu fälschen ist nahezu unmöglich. Wenn sich also ein Cookie-Dieb von einer
							anderen IP-Adresse als der beim Loginvorgang aktuellen aus einloggen will, wird ihm an dieser Stelle
							der Zugang verweigert und der Login muss erneut durchgeführt werden.
						*/
						if( !isset($_SESSION['ID']) OR $_SESSION['IPAddress'] !== $_SERVER['REMOTE_ADDR'] ) {
							// Fehlerfall | User ist nicht eingeloggt
if(DEBUG_F)				echo "<p class='debugAuth hint'><b>Line " . __LINE__ . "</b>: User ist nicht eingeloggt. <i>(" . basename(__FILE__) . ")</i></p>\n";	
							
							return false;

						#********** B) VALID LOGIN **********#
						} else {
							// Erfolgsfall | User ist eingeloggt
if(DEBUG_F)				echo "<p class='debugAuth ok'><b>Line " . __LINE__ . "</b>: User ist eingeloggt. <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
							/*
								SICHERHEIT: Um Cookiediebstahl oder Session Hijacking vorzubeugen, wird nach erfolgreicher
								Authentifizierung eine neue Session-ID vergeben. Ein Hacker, der zuvor ein Cookie mit einer 
								gültigen Session-ID erbeutet hat, kann dieses nun nicht mehr benutzen.
								Die Session-ID muss bei jedem erfolgreichem Login und bei jedem Logout erneuert werden, um
								einen effektiven Schutz zu bieten.
								
								Um die alte Session mit der alten (abgelaufenen) ID gleich zu löschen und eine neue Session
								mit einer neuen ID zu generieren, muss session_regenerate_id() den optionalen Parameter 
								delete_old_session=true erhalten.
							*/
							session_regenerate_id(true);
							
							// ID des eingeloggten Users als Integer zurückgeben
							return intval($_SESSION['ID']);
							
						} // CHECK FOR VALID LOGIN END
					
					} // SECURE PAGE ACCESS END					
				}


#********************************************************************************************#

				
				#****************************#
				#********** LOGOUT **********#
				#****************************#
				
				/**
				*
				*	Loggt einen User aus einer Session aus und löscht diese.
				*	Der ausgeloggte User wird auf eine optional zu übergebende URL umgeleitet.
				*	Der URL können optional zu übergebende Parameter angehängt werden
				*
				*	@param	String	$redirectPage='./'		Die URL,auf die umgeleitet werden soll
				*	@param	String	$urlParams=NULL			Die anzuhängenden URL-Parameter
				*
				*	@return	Void
				*
				*/
				function logout( $redirectPage='./', $urlParams=NULL ) {
if(DEBUG_F)		echo "<p class='debugAuth'>🌀 <b>Line " . __LINE__ . "</b>: Aufruf " . __FUNCTION__ . "('{$redirectPage}{$urlParams}') <i>(" . basename(__FILE__) . ")</i></p>\n";	
					
					#********** LOGOUT AND REDIRECT TO index.php **********#
					/*
						SICHERHEIT: Um Cookiediebstahl oder Session Hijacking vorzubeugen, wird nach erfolgreicher
						Authentifizierung eine neue Session-ID vergeben. Ein Hacker, der zuvor ein Cookie mit einer 
						gültigen Session-ID erbeutet hat, kann dieses nun nicht mehr benutzen.
						Die Session-ID muss bei jedem erfolgreichem Login und bei jedem Logout erneuert werden, um
						einen effektiven Schutz zu bieten.
						
						Um die alte Session mit der alten (abgelaufenen) gleich zu löschen, muss
						session_regenerate_id() den optionalen Parameter delete_old_session=true erhalten.
					*/
					
					session_regenerate_id(true);
					
					// 1. Session löschen
					session_destroy();					
					
					// 2. User auf öffentliche Seite umleiten und ggf. URL-Parameter übergeben
					header("Location: {$redirectPage}{$urlParams}");						
					// Fallback
					exit;					
				}


#********************************************************************************************#
?>