-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2022 at 08:18 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bloog_oop`
--
CREATE DATABASE IF NOT EXISTS `bloog_oop` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `bloog_oop`;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `blogID` int(11) NOT NULL,
  `blogHeadline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blogImagePath` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blogImageAlignment` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blogContent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `blogDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `catID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blogID`, `blogHeadline`, `blogImagePath`, `blogImageAlignment`, `blogContent`, `blogDate`, `catID`, `userID`) VALUES
(13, 'Apple', '../uploads/blogimages/283623031_r7kfzwpqnimgsv0th268_9-bju1dla5ceo4yx3_0978959001652183500.jpg', 'fright', 'An apple is an edible fruit produced by an apple tree.  Apple trees are cultivated worldwide and are the most widely grown species in the genus Malus.\r\n \r\nApples grown from seed tend to be very different from those of their parents, and the resultant fruit frequently lacks desired characteristics. \r\n', '2022-05-10 11:51:40', 8, 1),
(14, 'Orange', '../uploads/blogimages/911211590_wi7nsj8g9dkq2-ufvp5ro06mxt4czl3e_hbay1_0493884001652186558.jpg', 'fleft', 'An orange is a fruit of various citrus species in the family Rutaceae.\r\nOrange trees were found to be the most cultivated fruit tree in the world.\r\nOrange trees are widely grown in tropical and subtropical climates for their sweet fruit. The fruit of the orange tree can be eaten fresh, or processed for its juice or fragrant peel.\r\n', '2022-05-10 12:42:38', 8, 1),
(15, 'Rain', '../uploads/blogimages/619484642_21ylv8e49r0mbuh35zjt7_6gsnkcaixqdp-fwo_0385660001652187491.jpg', 'fleft', 'Rain is liquid water in the form of droplets that have condensed from atmospheric water vapor and then become heavy enough to fall under gravity.\r\nRain is a major component of the water cycle and is responsible for depositing most of the fresh water on the Earth.', '2022-05-10 12:58:11', 9, 1),
(16, 'Audi', NULL, 'fleft', 'The Audi Q3 is a subcompact luxury crossover SUV made by Audi. The Q3 has a transverse-mounted front engine, and entered production in 2011.', '2022-05-10 13:03:13', 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `catID` int(11) NOT NULL,
  `catLabel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`catID`, `catLabel`) VALUES
(8, 'Fruits'),
(9, 'lifestyle'),
(10, 'Car'),
(11, 'camera');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `userFirstName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userLastName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userEmail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userCity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userPassword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `userFirstName`, `userLastName`, `userEmail`, `userCity`, `userPassword`) VALUES
(1, 'abcd', 'efg', 'abcd@gmail.com', 'Berlin', '$2y$10$tbCYcuHF/flLur6pSSpMheR5DKA2io7T9TcE/Gw3Q/2aulfoQiGD2'),
(2, 'Bushaira', 'Chakkikalathil', 'abcd@ab.de', 'Fürth', '$2y$10$tbCYcuHF/flLur6pSSpMheR5DKA2io7T9TcE/Gw3Q/2aulfoQiGD2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blogID`),
  ADD KEY `blogs_ibfk_1` (`userID`),
  ADD KEY `blogs_ibfk_2` (`catID`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`catID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blogID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `catID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `blog_ibfk_2` FOREIGN KEY (`catID`) REFERENCES `category` (`catID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
